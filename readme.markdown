#About the NODE+Clima Application
=================================
You must run this script to setup up the dependent project. Further, this script 
will create a folder within this repositories' folder called 'Common'. This is a 
sub project that has common code shared by all NODE+ applications.



#Windows Setup
==============
	call git clone https://bitbucket.org/variabletech/node-clima.git

	cd node-clima
	
	call git clone http://bitbucket.org/variabletech/node-android-common.git
	
	rename node-android-common Common
	
	
#Unix / OSX Setup
	call git clone https://bitbucket.org/variabletech/node-clima.git

	cd node-clima
	
	call git clone http://bitbucket.org/variabletech/node-android-common.git

	mv node-android-common Common
	
	
