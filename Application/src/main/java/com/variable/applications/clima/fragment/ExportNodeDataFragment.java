package com.variable.applications.clima.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.variable.applications.clima.ClimaApplication;
import com.variable.applications.clima.DatalogDownloader;
import com.variable.applications.clima.R;
import com.variable.applications.clima.BaseApplication;
import com.variable.framework.dispatcher.DefaultNotifier;

import java.io.File;

/**
 * This fragment is responsible for offloading the datalogging from a NODE device.
 * Furthermore, this fragment allows sharing the data as a one time share to where ever the user decides.
 *
 * Created by coreymann on 8/5/13.
 */
public class ExportNodeDataFragment  extends Fragment {
    public static final String TAG = ExportNodeDataFragment.class.getName();

    private Button mDataLogOnButton;
    private Button mFetchButton;
    private DatalogDownloader mDLOGHelper;

    public static ExportNodeDataFragment newInstance(DatalogDownloader downloader){
        ExportNodeDataFragment frag= new ExportNodeDataFragment();
        frag.mDLOGHelper = downloader;
        frag.mDLOGHelper.setHandler(frag.mHandler);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View root =  inflater.inflate(R.layout.export_data,null, false);
        ClickListener listener = new ClickListener();
        mFetchButton  = (Button) root.findViewById(R.id.btnFetch);
        mFetchButton.setOnClickListener(listener);

        //Init the DataLog is on or off
        mDataLogOnButton = (Button) root.findViewById(R.id.btnControlDLog);
        mDataLogOnButton.setOnClickListener(listener);
        return root;
    }

    @Override
    public void onResume(){
        super.onResume();


        //Retrieve the Latest Status
        mDLOGHelper.requestCurrentStatus();

        //Check if datalogging is allowed.
        if(!((BaseApplication) getActivity().getApplication()).getActiveNode().getDatalogSettings().isAllowedState())
        {
            DatalogDownloader.showDatalogNotAllowedDialog(getActivity());
        }
        DefaultNotifier.instance().addDataLoggingListener(mDLOGHelper);
        DefaultNotifier.instance().addClimaPressureListener(mDLOGHelper);
        DefaultNotifier.instance().addClimaHumidityListener(mDLOGHelper);
        DefaultNotifier.instance().addClimaLightListener(mDLOGHelper);
        DefaultNotifier.instance().addClimaTemperatureListener(mDLOGHelper);
    }

    @Override
    public void onPause(){
        super.onPause();

        DefaultNotifier.instance().removeDataLoggingListener(mDLOGHelper);

        DefaultNotifier.instance().removeClimaPressureListener(mDLOGHelper);
        DefaultNotifier.instance().removeClimaHumidityListener(mDLOGHelper);
        DefaultNotifier.instance().removeClimaLightListener(mDLOGHelper);
        DefaultNotifier.instance().removeClimaTemperatureListener(mDLOGHelper);
    }

    public Handler getHandler() { return mHandler; }

    private void shareData(final File exportedData){
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(exportedData));
        getActivity().startActivity(Intent.createChooser(intent, "Share Exported Data to..."));

    }

    private class ClickListener  implements View.OnClickListener
    {
        @Override
        public void onClick(View view) {
            switch(view.getId()){
                case R.id.btnFetch:
                    mDLOGHelper.setRecordingEntity(((ClimaApplication) getActivity().getApplication()).getActiveRecording());
                    mDLOGHelper.beginExportData();
                    break;

                case R.id.btnControlDLog:
                try{    mDLOGHelper.flipDatalogEnableFlag(); }
                catch(Exception e){
                    DatalogDownloader.showDatalogNotAllowedDialog(getActivity());
                    return;
                }
            }
        }
    }

    private final Handler mHandler = new Handler(){
        ProgressDialog mProgressDialog;
        public void handleMessage(Message m){
            switch(m.what){
                case  DatalogDownloader.MESSAGE_DATALOG_STATUS:
                    switch(m.arg1){
                        case DatalogDownloader.ARGUMENT_NO_DATALOGGING:
                            mDataLogOnButton.setText("Start DataLogging");
                            break;
                        case DatalogDownloader.ARGUMENT_DATALOG_STATUS_RUNNING:
                            mDataLogOnButton.setText("Stop Datalogging");
                            break;
                    }
                    break;

                case  DatalogDownloader.MESSAGE_DATALOG_UPDATE:
                    switch(m.arg1){
                        case DatalogDownloader.ARGUMENT_NO_RECORDS_FETCHED:
                            Toast.makeText(getActivity(), "No Records Fetched", Toast.LENGTH_SHORT).show();
                            break;

                        case DatalogDownloader.ARGUMENT_SHARE_DATA:
                             File dataFile = new File(m.obj.toString());
                             shareData(dataFile);
                             break;

                        case DatalogDownloader.ARGUMENT_FETCHED_RECORDS_UPDATE:
                            if(mProgressDialog != null){
                                mProgressDialog.setMessage("Fetched Records: " + m.arg2);
                            }
                            break;
                        case DatalogDownloader.START_DATALOGGING_FETCH_ARGUMENT: //Start Datalogging
                            mFetchButton.setEnabled(false);
                            mProgressDialog = new ProgressDialog(getActivity());
                            mProgressDialog.setCancelable(true);
                            mProgressDialog.setTitle("Datalogging Operation");
                            mProgressDialog.setMessage("Fetching Records");
                            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mDLOGHelper.cancelExport();
                                }
                            });
                            mProgressDialog.show();
                            break;
                        case DatalogDownloader.FETCH_COMPLETED: //Stop Datalogging.
                            mFetchButton.setEnabled(true);
                            if(mProgressDialog != null){
                                mProgressDialog.dismiss();
                                mProgressDialog = null;
                            }
                            break;
                    }
            }

        }
    };
}
