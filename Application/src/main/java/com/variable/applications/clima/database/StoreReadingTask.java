package com.variable.applications.clima.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import com.variable.applications.clima.database.ClimaDBHelper;

import java.util.Queue;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Corey_2 on 1/22/14.
 */
public class StoreReadingTask extends TimerTask {
    public final Queue<ContentValues> ReadingValues = new ConcurrentLinkedQueue<ContentValues>();
    private final String tableName;

    public StoreReadingTask(Context context, String tableName) {
        this.context = context;
        this.tableName = tableName;
    }

    private Context context;

    @Override
    public void run() {
        SQLiteDatabase db = ClimaDBHelper.instance(context).getWritableDatabase();
        while(!ReadingValues.isEmpty())
        {
            ContentValues values = ReadingValues.poll();
            db.insert(tableName, null, values);
        }
        db.close();
    }
}
