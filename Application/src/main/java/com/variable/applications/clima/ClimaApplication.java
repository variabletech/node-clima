package com.variable.applications.clima;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.variable.applications.clima.database.ClimaContract;
import com.variable.applications.clima.database.ClimaDBHelper;
import com.variable.applications.clima.database.ClimaDeviceEntity;
import com.variable.applications.clima.database.RecordingEntity;
import com.variable.applications.clima.database.StoreReadingTask;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.adapter.StatusAdapter;
import com.variable.framework.node.reading.SensorReading;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

/**
 * Created by Corey_2 on 10/2/13.
 */
public class ClimaApplication extends BaseApplication implements ClimaSensor.ClimaHumidityListener, ClimaSensor.ClimaTemperatureListener, ClimaSensor.ClimaPressureListener, ClimaSensor.ClimaLightListener {
    public static final String OUTPUT_TIME_FORMAT_KEY = "com.variable.preference.output_time_format";
    private RecordingEntity activeRecording;
    private ClimaDeviceEntity currentDeviceEntity;

    private Timer recordingTimer;
    private StoreReadingTask recordingTask;


    @Override
    public void onCreate() {
        super.onCreate();

        //Init Clima DB Helper
        SQLiteDatabase db = ClimaDBHelper.instance(this).getReadableDatabase();
        Cursor c =  db.query(
                ClimaContract.Recordings.TABLE_NAME,
                new String[] { ClimaContract.Recordings.ID,  ClimaContract.Recordings.RECORDING_NAME, ClimaContract.Recordings.CREATION_DATE},
                ClimaContract.Recordings.ID + " = 1 ",
                null,
                null,
                null,
                null
        );

        try {
            c.moveToFirst();
            activeRecording = new RecordingEntity(c.getInt(0), c.getString(1), new Date(c.getLong(2)));
            db.close();
        }catch (Exception e){}

        recordingTask = new StoreReadingTask(this, ClimaContract.ClimaReadings.TABLE_NAME);
        recordingTimer = new Timer();
        recordingTimer.scheduleAtFixedRate(recordingTask, 0 , 5000);

    }

    @Override
    public void onRecordingStart() {
        super.onRecordingStart();

        DefaultNotifier.instance().addClimaHumidityListener(this);
        DefaultNotifier.instance().addClimaLightListener(this);
        DefaultNotifier.instance().addClimaPressureListener(this);
        DefaultNotifier.instance().addClimaTemperatureListener(this);
    }

    @Override
    public void onRecordingStop() {
        super.onRecordingStop();

        DefaultNotifier.instance().removeClimaHumidityListener(this);
        DefaultNotifier.instance().removeClimaLightListener(this);
        DefaultNotifier.instance().removeClimaPressureListener(this);
        DefaultNotifier.instance().removeClimaTemperatureListener(this);
    }

    public void setCurrentDeviceEntity(ClimaDeviceEntity entity)
    {
        this.currentDeviceEntity = entity;
    }

    @Override
    public RecordingEntity getActiveRecording() {
        return activeRecording;
    }

    public void setRecordingEntity(RecordingEntity entity) { activeRecording = entity; }

    @Override
    public int removeReadings(List<RecordingEntity> recordings) {
        StringBuilder filteringClause = new StringBuilder("(");
        for(int i=0; i< recordings.size() - 1; i++){
            filteringClause.append(recordings.get(i).getId());
            filteringClause.append(",");
        }
        filteringClause.append(recordings.get(recordings.size() - 1).getId());
        filteringClause.append(")");


        SQLiteDatabase db = ClimaDBHelper.instance(this).getWritableDatabase();
        int rowsDeleted = db.delete(
                ClimaContract.ClimaReadings.TABLE_NAME,
                ClimaContract.ClimaReadings.RECORDING_ID + " IN  " + filteringClause,
                null
        );
        db.close();

        return rowsDeleted;
    }

    @Override
    public AsyncTask<List<RecordingEntity>, Boolean, Boolean> beginAsyncReadingRetrieving(final File f) {
        return new AsyncTask<List<RecordingEntity>, Boolean, Boolean>(){

            @Override
            protected Boolean doInBackground(List<RecordingEntity>... lists) {
                Boolean results = true;
                BufferedWriter fw = null;

                for(List<RecordingEntity> list : lists){
                    Cursor c = null;
                    try{
                        //Retrieve Readings
                        c = ClimaDBHelper.instance(ClimaApplication.this).loadReadingsByRecording(list);

                        //Move to the First Position.
                        if(c != null && (!c.moveToFirst() || c.getCount() == 0)){   return false;   }

                        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm.ss");
                        SimpleDateFormat timeZoneFormat = new SimpleDateFormat(" z ");
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                        int output_time_format = PreferenceManager.getDefaultSharedPreferences(ClimaApplication.this).getBoolean(ClimaApplication.OUTPUT_TIME_FORMAT_KEY, false) ? 1 : 0;

                        //Format Readings
                        fw = new BufferedWriter(new FileWriter(f), 4096);
                        String header =
                                "# "
                                        + ((output_time_format == 0 ) ? " Date Received, Time Received,  Time Zone," : " epoch time," )
                                        + "Value,"
                                        + "Unit,"
                                        + "Clima Serial,"
                                        + (list.size() > 1 ? " Group \r\n" : "\r\n");
                        fw.append(header);


                        while(c.getCount() - 1 != c.getPosition() + 1){


                            //Write Line to File
                            Date date = new Date(c.getLong(0));
                            String line =
                                    (output_time_format == 0 ? dateFormat.format(date) + " , " + timeFormat.format(date)  + " , " + timeZoneFormat.format(date) : date.getTime()) + " , " +
                                            c.getFloat(1)    + " , " +
                                            ClimaContract.getUnitSymbol(c.getInt(2))   + " , " +
                                            c.getString(3)   + " , " +
                                            (list.size() > 1 ? " , " +  c.getString(4) : "" ) + "\r\n";
                            fw.append(line);

                            //Check if task is canceled.
                            if(isCancelled()){  f.delete(); return false; }

                            c.moveToNext();

                        }

                        results &= true;

                    }catch(IOException e){
                        e.printStackTrace();
                        f.delete();
                        return false;
                    }catch(IllegalStateException e2){
                        e2.printStackTrace();
                        f.delete();
                        return false;
                    }catch(CursorIndexOutOfBoundsException cursorExc){
                        //Unable to determine why this exception occurs but if it does just process as normal and succesful.
                        cursorExc.printStackTrace();
                        return true;
                    } finally{
                        if(c != null){
                            c.close();
                        }

                        if(fw != null){
                            try {   fw.close(); } catch(IOException ignored){}
                        }
                    }
                }

                return results;
            }
        };
    }

    @Override
    public AsyncTask<LinkedList<RecordingEntity>, Void, Void> beginAsyncRecordingRetrieving() {
        return new AsyncTask<LinkedList<RecordingEntity>, Void, Void>() {

            @Override
            protected Void doInBackground(LinkedList<RecordingEntity>...lists) {
                for(LinkedList<RecordingEntity> list : lists){
                    LinkedList<RecordingEntity> recordings =   ClimaDBHelper.instance(ClimaApplication.this).getRecordings();
                    list.addAll(recordings);
                }
                return null;
            }
        };
    }

    @Override
    public void onClimaHumidityUpdate(ClimaSensor climaSensor, SensorReading<Float> reading) {
        ContentValues values = new ContentValues();
        values.put(ClimaContract.ClimaReadings.CREATION_DATE, reading.getTimeStamp().getTime());
        values.put(ClimaContract.ClimaReadings.CLIMA_DEVICE_ID, currentDeviceEntity.getId());
        values.put(ClimaContract.ClimaReadings.DATA_TYPE_ID, ClimaContract.ClimaDataTypes.HUMIDITY_DATA_TYPE_ID);
        values.put(ClimaContract.ClimaReadings.READING, formatter.format(reading.getValue()));
        values.put(ClimaContract.ClimaReadings.RECORDING_ID, activeRecording.getId());
        recordingTask.ReadingValues.add(values);
    }
    NumberFormat formatter = new DecimalFormat("#.##");

    @Override
    public void onClimaTemperatureUpdate(ClimaSensor climaSensor, SensorReading<Float> reading) {
        ContentValues values = new ContentValues();
        values.put(ClimaContract.ClimaReadings.CREATION_DATE, reading.getTimeStamp().getTime());
        values.put(ClimaContract.ClimaReadings.CLIMA_DEVICE_ID, currentDeviceEntity.getId());
        values.put(ClimaContract.ClimaReadings.DATA_TYPE_ID, ClimaContract.ClimaDataTypes.TEMPERATURE_DATA_TYPE_ID);
        values.put(ClimaContract.ClimaReadings.READING, formatter.format(reading.getValue()));
        values.put(ClimaContract.ClimaReadings.RECORDING_ID, activeRecording.getId());
        recordingTask.ReadingValues.add(values);
    }

    @Override
    public void onClimaPressureUpdate(ClimaSensor climaSensor, SensorReading<Integer> reading) {
        ContentValues values = new ContentValues();
        values.put(ClimaContract.ClimaReadings.CREATION_DATE, reading.getTimeStamp().getTime());
        values.put(ClimaContract.ClimaReadings.CLIMA_DEVICE_ID, currentDeviceEntity.getId());
        values.put(ClimaContract.ClimaReadings.DATA_TYPE_ID, ClimaContract.ClimaDataTypes.PRESSURE_DATA_TYPE_ID);
        values.put(ClimaContract.ClimaReadings.READING, formatter.format(reading.getValue()));
        values.put(ClimaContract.ClimaReadings.RECORDING_ID, activeRecording.getId());
        recordingTask.ReadingValues.add(values);
    }

    @Override
    public void onClimaLightUpdate(ClimaSensor climaSensor, SensorReading<Float> reading) {
        ContentValues values = new ContentValues();
        values.put(ClimaContract.ClimaReadings.CREATION_DATE, reading.getTimeStamp().getTime());
        values.put(ClimaContract.ClimaReadings.CLIMA_DEVICE_ID, currentDeviceEntity.getId());
        values.put(ClimaContract.ClimaReadings.DATA_TYPE_ID, ClimaContract.ClimaDataTypes.LIGHT_DATA_TYPE_ID);
        values.put(ClimaContract.ClimaReadings.READING, formatter.format(reading.getValue()));
        values.put(ClimaContract.ClimaReadings.RECORDING_ID, activeRecording.getId());
        recordingTask.ReadingValues.add(values);
    }

}
