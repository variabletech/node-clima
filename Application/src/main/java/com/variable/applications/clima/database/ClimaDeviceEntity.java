package com.variable.applications.clima.database;

/**
 * Created by Corey_2 on 9/6/13.
 */
public class ClimaDeviceEntity {

    private long id;
    private String serial;

    public ClimaDeviceEntity(long id, String serial) {
        this.id = id;
        this.serial = serial;
    }

    public long getId() {
        return id;
    }

    public String getSerial() {
        return serial;
    }
}
