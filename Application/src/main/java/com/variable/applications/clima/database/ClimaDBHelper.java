package com.variable.applications.clima.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Corey_2 on 9/5/13.
 */
public class ClimaDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Clima Data";
    public static final int DATABASE_VERSION = 2;
    public static  ClimaDBHelper INSTANCE;
    public static synchronized final ClimaDBHelper instance(Context context) {
        if(INSTANCE == null)
            INSTANCE = new ClimaDBHelper(context);

        return INSTANCE;
    }

    private static final String LONG_TYPE = " LONG ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String COMMA_SEP = " , ";

    private static final  String CREATE_RECORDINGS_TABLE =
                    " CREATE TABLE " + ClimaContract.Recordings.TABLE_NAME + " ( " +
                            ClimaContract.Recordings.ID + INTEGER_TYPE + " PRIMARY KEY " + COMMA_SEP +
                            ClimaContract.Recordings.RECORDING_NAME + " VARCHAR(100) "   + COMMA_SEP +
                            ClimaContract.Recordings.CREATION_DATE  + " DATETIME "       + " );";

    private static final String CREATE_CLIMA_DATA_TYPES_TABLE =
                    " CREATE TABLE " + ClimaContract.ClimaDataTypes.TABLE_NAME + " ( " +
                            ClimaContract.ClimaDataTypes.ID + INTEGER_TYPE + " PRIMARY KEY " + COMMA_SEP +
                            ClimaContract.ClimaDataTypes.DATA_TYPE_NAME + " VARCHAR(25) " + ");";

    private static final String CREATE_CLIMA_DEVICES_TABLE =
                    " CREATE TABLE " + ClimaContract.ClimaDevices.TABLE_NAME + " ( " +
                            ClimaContract.ClimaDevices.ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT " + COMMA_SEP +
                            ClimaContract.ClimaDevices.SERIAL_NUMBER + " VARCHAR(30) " + " ); ";

    private static final String CREATE_CLIMA_READINGS_TABLE =
                    " CREATE TABLE " + ClimaContract.ClimaReadings.TABLE_NAME + " ( " +
                            ClimaContract.ClimaReadings.ID                  + INTEGER_TYPE + " PRIMARY KEY " +  COMMA_SEP   +
                            ClimaContract.ClimaReadings.CLIMA_DEVICE_ID     + INTEGER_TYPE                   + COMMA_SEP    +
                            ClimaContract.ClimaReadings.DATA_TYPE_ID        + INTEGER_TYPE +                    COMMA_SEP   +
                            ClimaContract.ClimaReadings.RECORDING_ID        + INTEGER_TYPE +                    COMMA_SEP   +
                            ClimaContract.ClimaReadings.UNIT_TYPE_ID        + INTEGER_TYPE +                    COMMA_SEP   +
                            ClimaContract.ClimaReadings.READING             + " REAL "     +                    COMMA_SEP   +
                            ClimaContract.ClimaReadings.CREATION_DATE       + " DATETIME " +                    COMMA_SEP   +
                            " FOREIGN KEY(" +ClimaContract.ClimaReadings.CLIMA_DEVICE_ID + ") REFERENCES " + ClimaContract.ClimaDevices.TABLE_NAME + "(" + ClimaContract.ClimaDevices.ID + " )" +
                            " FOREIGN KEY(" +ClimaContract.ClimaReadings.RECORDING_ID + ") REFERENCES " + ClimaContract.Recordings.TABLE_NAME + "(" + ClimaContract.Recordings.ID + " )" +
                            " FOREIGN KEY(" +ClimaContract.ClimaReadings.DATA_TYPE_ID + ") REFERENCES " + ClimaContract.ClimaDataTypes.TABLE_NAME + "(" + ClimaContract.ClimaDataTypes.ID + " )" +
                                             " ); ";


    private static final String POPULATE_DATA_TYPES =
            "INSERT INTO " + ClimaContract.ClimaDataTypes.TABLE_NAME + " ( " + ClimaContract.ClimaDataTypes.DATA_TYPE_NAME + " ) " +
                    "     SELECT 'Humidity'" +
                    "       UNION" +
                    "     SELECT 'Pressure'" +
                    "       UNION" +
                    "     SELECT  'Temperature'" +
                    "       UNION " +
                    "    SELECT  'Light' ";

    private ClimaDBHelper(Context c){
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_RECORDINGS_TABLE);
        sqLiteDatabase.execSQL(CREATE_CLIMA_DEVICES_TABLE);
        sqLiteDatabase.execSQL(CREATE_CLIMA_DATA_TYPES_TABLE);
        sqLiteDatabase.execSQL(CREATE_CLIMA_READINGS_TABLE);

        sqLiteDatabase.execSQL(POPULATE_DATA_TYPES);

        sqLiteDatabase.execSQL("INSERT INTO " + ClimaContract.Recordings.TABLE_NAME +
                " ( " + ClimaContract.Recordings.RECORDING_NAME +  " , " + ClimaContract.Recordings.CREATION_DATE + " ) VALUES " +
                " ( 'My Clima History' , '" +
                new Date().getTime() +
                "'); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if(newVersion == 1){
            onCreate(sqLiteDatabase);
        }else if(newVersion == 2){
            sqLiteDatabase.execSQL("DROP TABLE " + ClimaContract.ClimaDataTypes.TABLE_NAME);
            sqLiteDatabase.execSQL(CREATE_CLIMA_DATA_TYPES_TABLE);
            sqLiteDatabase.execSQL(POPULATE_DATA_TYPES);
        }
    }

    public void saveReading(long device_id, int type, float reading, Date recievedReading,  int recording_id){
        ContentValues values = new ContentValues();
                      values.put(ClimaContract.ClimaReadings.DATA_TYPE_ID, type);
                      values.put(ClimaContract.ClimaReadings.READING, reading);
                      values.put(ClimaContract.ClimaReadings.CREATION_DATE, recievedReading.getTime());
                      values.put(ClimaContract.ClimaReadings.RECORDING_ID, recording_id);
                      values.put(ClimaContract.ClimaReadings.CLIMA_DEVICE_ID, device_id);

        getWritableDatabase().insert(ClimaContract.ClimaReadings.TABLE_NAME, null, values);
    }

    public ClimaDeviceEntity insertOrGetClimaDevice(String serial){
        ContentValues values = new ContentValues();
        values.put(ClimaContract.ClimaDevices.SERIAL_NUMBER, serial);

       long id =  getReadableDatabase().replace(ClimaContract.ClimaDevices.TABLE_NAME, null, values);
       return new ClimaDeviceEntity(id, serial);
    }

    public RecordingEntity insertRecording(String name){
        ContentValues values = new ContentValues();
            values.put(ClimaContract.Recordings.CREATION_DATE, new Date().getTime());
            values.put(ClimaContract.Recordings.RECORDING_NAME, name);

        int id =  (int) getWritableDatabase().insert(ClimaContract.Recordings.TABLE_NAME, null, values);
        return new RecordingEntity(id, name, new Date());
    }


    public LinkedList<RecordingEntity> getRecordings(){
        LinkedList<RecordingEntity> recordings = new LinkedList<RecordingEntity>();
        try{
            SQLiteDatabase db = getReadableDatabase();
            Cursor c =  db.query(
                    ClimaContract.Recordings.TABLE_NAME,
                    new String[] { ClimaContract.Recordings.ID, ClimaContract.Recordings.RECORDING_NAME, ClimaContract.Recordings.CREATION_DATE },
                    null,
                    null,
                    null,
                    null,
                    ClimaContract.Recordings.CREATION_DATE);


            if(!c.moveToFirst()) { return recordings; }

            do{
                recordings.add(new RecordingEntity(c.getInt(0), c.getString(1),new Date(c.getLong(2))));
            }while(c.moveToNext());

            c.close();
            db.close();
        }catch(Exception e) { e.printStackTrace(); }

        return recordings;
    }

    public Cursor loadReadingsByRecording(List<RecordingEntity> recordings) {
        StringBuilder filteringClause = new StringBuilder();
        for(int i = 0; i < recordings.size() - 1; i++){
            filteringClause.append(recordings.get(i).getId());
            filteringClause.append(" , ");
        }
        filteringClause.append(recordings.get(recordings.size() - 1).getId()); //Obtain the Last Reading without ,

        String query = "SELECT " +
                ClimaContract.ClimaReadings.TABLE_NAME + "." + ClimaContract.ClimaReadings.CREATION_DATE + " , " +
                ClimaContract.ClimaReadings.READING          + " , " +
                ClimaContract.ClimaDataTypes.TABLE_NAME + "." + ClimaContract.ClimaDataTypes.ID + " , " +
                ClimaContract.ClimaDevices.SERIAL_NUMBER     + " , " +
                ClimaContract.Recordings.RECORDING_NAME      +
        " FROM " +
                ClimaContract.ClimaReadings.TABLE_NAME +
                " INNER JOIN " + ClimaContract.ClimaDevices.TABLE_NAME +
                " ON " + ClimaContract.ClimaDevices.TABLE_NAME  + "." + ClimaContract.ClimaDevices.ID + " = " + ClimaContract.ClimaReadings.TABLE_NAME + "." + ClimaContract.ClimaReadings.CLIMA_DEVICE_ID +
                " INNER JOIN " + ClimaContract.Recordings.TABLE_NAME    +
                " ON " +   ClimaContract.Recordings.TABLE_NAME  + "." + ClimaContract.Recordings.ID + " = " + ClimaContract.ClimaReadings.TABLE_NAME + "." + ClimaContract.ClimaReadings.RECORDING_ID +
                " INNER JOIN " +   ClimaContract.ClimaDataTypes.TABLE_NAME +
                " ON " + ClimaContract.ClimaDataTypes.TABLE_NAME  + "." + ClimaContract.ClimaDataTypes.ID + " = " + ClimaContract.ClimaReadings.TABLE_NAME + "." + ClimaContract.ClimaReadings.DATA_TYPE_ID +
                " WHERE " +   ClimaContract.Recordings.TABLE_NAME + "." + ClimaContract.Recordings.ID + " IN ( " + filteringClause.toString() + " )";

        //Get or Wait for an open connection.
        SQLiteDatabase db = getWritableDatabase();
        return db.rawQuery(query, null);
    }
}
