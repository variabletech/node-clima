package com.variable.applications.clima;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ExpandableListView;

import com.variable.applications.clima.adapters.ExpandableListAdapter;
import com.variable.applications.clima.database.ClimaDBHelper;
import com.variable.applications.clima.database.ClimaDeviceEntity;
import com.variable.applications.clima.fragment.ClimaGraphFragment;
import com.variable.applications.clima.fragment.ClimaSettingsFragment;
import com.variable.applications.clima.fragment.ExportNodeDataFragment;

import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.DataLogSetting;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;


public class SplashActivity extends BaseActivity {
    public static final String EXPORT_DATA_GROUP_KEY = "Export Data";

    //TODO: Add Splash Background...

    private ExpandableListAdapter mListAdapter;


    @Override
    public void onDisconnect(NodeDevice node){
        super.onDisconnect(node);

        try{
            getFragmentManager().popBackStackImmediate();
        }catch(IllegalStateException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSettingsMenuItemPressed() {
        animateToFragment(new ClimaSettingsFragment(), ClimaSettingsFragment.TAG);
    }

    @Override
    public void onCommunicationInitCompleted(final NodeDevice node){
        super.onCommunicationInitCompleted(node);


        //Check for Clima Module
        if(ClimaSingleton.containsClima(node)){

            ClimaDeviceEntity entity = ClimaDBHelper.instance(this).insertOrGetClimaDevice(node.findSensor(NodeEnums.ModuleType.CLIMA).getSerialNumber());
            ((ClimaApplication) getApplication()).setCurrentDeviceEntity(entity);

            mListAdapter.addGroupItem(EXPORT_DATA_GROUP_KEY);
            setTitle(getString(R.string.clima));
            animateToFragment(new ClimaGraphFragment(), ClimaGraphFragment.TAG);
        }else{
           ClimaSingleton.showSensorNotFound(this,node);
        }


    }

    /**
     * Handles Export Data by adding a new ExportNodeDataFragment.
     * @param name
     * @return
     */
    @Override
    public boolean onGroupItemSelected(String name) {
        if(name.equals(EXPORT_DATA_GROUP_KEY)){
            NodeDevice node = ((BaseApplication) getApplication()).getActiveNode();
            ClimaSensor sensor = node.findSensor(NodeEnums.ModuleType.CLIMA);
            ClimaDeviceEntity entity = ClimaDBHelper.instance(this).insertOrGetClimaDevice(sensor.getSerialNumber());
            DatalogDownloader helper = new DatalogDownloader(node.getDatalogSettings(), DataLogSetting.DataType.DataTypeClima, entity, this);
            ExportNodeDataFragment fetchDlogFrag =  ExportNodeDataFragment.newInstance(helper);
            animateToFragment(fetchDlogFrag, ExportNodeDataFragment.TAG);
            return true;
        }else{
            return super.onGroupItemSelected(name);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mListAdapter = (ExpandableListAdapter)((ExpandableListView) findViewById(R.id.left_drawer)).getExpandableListAdapter();
        PreferenceManager.setDefaultValues(this, com.variable.applications.clima.R.xml.preferences, false);
    }

    /**
     *
     * @return true, if the user preferences for auto-connect is enabled.
     */
    @Override
    public boolean isAutoConnect()  {    return true;    }

    @Override
    public void onSelectedApp(String appName){
        if(appName.equals(getString(R.string.clima))){  return; }
        else{       super.onSelectedApp(appName); }
    }
}


