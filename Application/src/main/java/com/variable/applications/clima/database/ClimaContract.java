package com.variable.applications.clima.database;

import android.provider.BaseColumns;

/**
 * Created by Corey_2 on 9/5/13.
 */
public abstract class ClimaContract {

    public abstract class ClimaDevices implements BaseColumns{
        public static final String TABLE_NAME = "ClimaDevices";
        public static final String ID = "id";
        public static final String SERIAL_NUMBER = "serial_number";
        //TODO: Add Node Serial Number
    }

    /**
     * This lookup table contains the distinct names of all the records.
     */
    public abstract class Recordings implements BaseColumns{
        public static final String TABLE_NAME = "Recordings";
        public static final String ID = "id";
        public static final String RECORDING_NAME = "recoding_name";
        public static final String CREATION_DATE  = "creation_date";
    }

    /**
     * This Lookup Table contains all the data types for clima. I.E. Light, Temperature....
     */
    public abstract class ClimaDataTypes implements BaseColumns{
        public static final String TABLE_NAME = "ClimaDataTypes";
        public static final String ID = "id";
        public static final String DATA_TYPE_NAME = "data_type_name";
        public static final int LIGHT_DATA_TYPE_ID = 4;
        public static final int PRESSURE_DATA_TYPE_ID = 2;
        public static final int TEMPERATURE_DATA_TYPE_ID = 3;
        public static final int HUMIDITY_DATA_TYPE_ID = 1;



    }

    public abstract class ClimaReadings implements BaseColumns{
        public static final String TABLE_NAME = "ClimaReadings";
        public static final String ID                   = "id";
        public static final String CLIMA_DEVICE_ID      = "clima_device_id";
        public static final String RECORDING_ID         = "recording_id";
        public static final String DATA_TYPE_ID         = "data_type_id";
        public static final String UNIT_TYPE_ID         = "unit_type_id";
        public static final String READING              = "reading";
        public static final String CREATION_DATE        = "creation_date";
    }



    public static String getUnitSymbol(int type){
        switch(type)
        {
            case ClimaDataTypes.LIGHT_DATA_TYPE_ID:
                return "lux";
            case ClimaDataTypes.PRESSURE_DATA_TYPE_ID:
                return "kPa";
            case ClimaDataTypes.TEMPERATURE_DATA_TYPE_ID:
                return "C";
            case ClimaDataTypes.HUMIDITY_DATA_TYPE_ID:
                return "%RH";
            default:
                return "NONE";
        }
    }
}

