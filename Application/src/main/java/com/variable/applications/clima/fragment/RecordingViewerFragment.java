package com.variable.applications.clima.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.variable.applications.clima.R;
import com.variable.applications.clima.adapters.HistoryListAdapter;
import com.variable.applications.clima.BaseApplication;
import com.variable.applications.clima.database.RecordingEntity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class RecordingViewerFragment extends Activity {
    public static final String TAG = RecordingViewerFragment.class.getName();

    /**
     * Message that signifies the recording groups have been retrieved.
     * When this message is sent the object sent with it needs to be a List of RecordingEntities.
     */
    public static final int MESSAGE_RETRIEVED_RECORDING_GROUPS = 0;
    /**
     * Message that signifies the readings that correspond to the specified recordings have been inserted into a file.
     * When this message is sent the object sent with it needs to be a java.io.File type.
     */
    public static final int MESSAGE_CREATED_RECORDING_FILE     = 1;

    /**
     * This constant is used to lookup the name of the data file to be created. Additionally, the time in the format hh.mm.ss will be appended to the
     * end of this value.
     */
    public static final String PREFERENCE_DATA_FILE_NAME = "com.variable.preference.DATA_FILE_NAME";

    /**
     * This constants is used to lookup up a directory starting with a name and ending with / that will be appended to the base directory for the
     * data export file.
     */
    public static final String PREFERENCE_EXTRA_DIRECTORY_NAME = "com.variable.preference.EXTRA_DIRECTORY_NAME";

    private ListView listView;
    private HistoryListAdapter arrayAdapter;
    private EditText inputSearch;
    private ProgressBar progressBar;
    private ProgressDialog mProgressDialog;

    private AsyncTask<List<RecordingEntity>, Boolean, Boolean> mReadingTask;
    private AsyncTask<LinkedList<RecordingEntity>, Void, Void> mRecordingTask;

    private boolean displayOptionsMenu = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history);

        listView = (ListView) findViewById(R.id.history_list_view);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        initiateRecordingRetrieval();

        //Setting up the search functionality
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
               RecordingViewerFragment.this.arrayAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {   }

        });
    }

    @Override
    public void onPause(){
        super.onPause();

        if(mRecordingTask != null && mRecordingTask.getStatus() != AsyncTask.Status.FINISHED){
            mRecordingTask.cancel(true);
        }

        if(mReadingTask != null && mReadingTask.getStatus() != AsyncTask.Status.FINISHED){
            mReadingTask.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recording, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){

        menu.findItem(R.id.action_remove).setVisible(displayOptionsMenu);
        menu.findItem(R.id.action_share).setVisible(displayOptionsMenu);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.action_share){

            ArrayList<RecordingEntity> exportRecordings = arrayAdapter.getCheckedRecordings();
            if(exportRecordings.size() == 0){
                Toast.makeText(this, "Select recording group(s) to be exported", Toast.LENGTH_SHORT).show();
                return true;
            }

            initiateReadingRetrieval(exportRecordings);
            return true;
        }else if(item.getItemId() == R.id.action_remove){
            final ArrayList<RecordingEntity> recordings = arrayAdapter.getCheckedRecordings();
            if(recordings.size() == 0){
                Toast.makeText(this, "Nothing to be removed.", Toast.LENGTH_SHORT).show();
                return true;
            }

            new AlertDialog.Builder(this)
                    .setTitle("Confirmation")
                    .setMessage("Are you sure you wish continue and remove  all readings belonging to these " + recordings.size() + "  recordings?")
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            removeReadings(recordings);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .create().show();
        }

        return false;
    }


    /**
     * Initiates an operation to retrieve all the recording groups.
     * This operation is finished by sending the RecordingFragments.MESSAGE_RETRIEVED_RECORDING_GROUPS  with the message's object
     * being the List of RecordingEntitys' retrieved.
     */
    protected  void initiateRecordingRetrieval(){
        //Populate List View
        final LinkedList<RecordingEntity> recordings = new LinkedList<RecordingEntity>();
        mRecordingTask = ((BaseApplication) getApplication()).beginAsyncRecordingRetrieving();
        mRecordingTask.execute(recordings);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {

                    mRecordingTask.get();
                    mHandler.obtainMessage(MESSAGE_RETRIEVED_RECORDING_GROUPS, recordings).sendToTarget();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initiates an operation to retrieve all readings that are contained in the specified recording groups.
     * Furthermore, this operation is finished by sending the RecordingActivity.MESSAGE_CREATED_RECORDINGS_FILE with the message's object set to the path and name of a file
     * where all the recordings are located.
     */
    protected void initiateReadingRetrieval(List<RecordingEntity> recordings){

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Exporting Data");
        mProgressDialog.setMessage("Getting Captured Data   (may take some time)");
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Stop Export", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mReadingTask.cancel(true);
            }
        });
        mProgressDialog.show();

        final File f = createDataFile();
        mReadingTask = ((BaseApplication) getApplication()).beginAsyncReadingRetrieving(f);
        mReadingTask.execute(recordings);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean didRetrieve = mReadingTask.get();
                    if(didRetrieve){
                        mHandler.obtainMessage(MESSAGE_CREATED_RECORDING_FILE,f).sendToTarget();
                    }else{
                        Toast.makeText(RecordingViewerFragment.this, "We did not find any recordings.", Toast.LENGTH_SHORT).show();
                    }
                    mProgressDialog.cancel();
                    mProgressDialog = null;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Initiates removing all readings belonging to the recordings in the list.
     * @param recordings
     */
    protected void removeReadings(List<RecordingEntity> recordings){
        int rowsDeletect = ((BaseApplication) getApplication()).removeReadings(recordings);
        Toast.makeText(this, "Removed " + rowsDeletect + " Readings", Toast.LENGTH_SHORT).show();
    }

    private void shareData(final File exportedData){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/csv");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(exportedData));
        startActivity(Intent.createChooser(intent, "Share Exported Data to..."));
        finish();
    }
    protected  final Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg){
            if(msg.what == MESSAGE_RETRIEVED_RECORDING_GROUPS){
                progressBar.setVisibility(View.GONE);

                arrayAdapter = new HistoryListAdapter(RecordingViewerFragment.this, (List<RecordingEntity>) msg.obj);
                listView.setAdapter(arrayAdapter);
                arrayAdapter.setSelectedListener(new HistoryListAdapter.SelectedListener() {
                   @Override
                   public void firstSelectedItem() {
                       displayOptionsMenu = true;
                       invalidateOptionsMenu();
                   }

                   @Override
                   public void noSelectedItem() {
                       displayOptionsMenu = false;
                       invalidateOptionsMenu();
                   }
               });

            }else if(msg.what == MESSAGE_CREATED_RECORDING_FILE){
                progressBar.setVisibility(View.GONE);
                if(msg.obj instanceof File){
                    shareData((File) msg.obj);
                }else{
                    Log.d(TAG, "messags object must be of file type");
                }
            }
        }
    };

    /**
     * Creates a Data file for the readings.
     * Further it looks up the base file name using the PREFERENCE_DATA_FILE_NAME, if it is not present it will default to exportedData.
     * Then a small timestamp formatted hh.mm.ss will be appended to the file. This file will be created on the storage device.
     * @return
     */
    protected  File createDataFile(){
        //Create the FilePath.
        SimpleDateFormat fileNameFormatter = new SimpleDateFormat("hh.mm.ss");
        String baseFileName = PreferenceManager.getDefaultSharedPreferences(this).getString(PREFERENCE_DATA_FILE_NAME, "exportedData");
        String extraDirectory = PreferenceManager.getDefaultSharedPreferences(this).getString(PREFERENCE_EXTRA_DIRECTORY_NAME, "");
        String fileName =  baseFileName + "-" + fileNameFormatter.format(new Date().getTime()) + ".csv";
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + "/Variable/" + extraDirectory);

        dir.mkdirs();

        File file =  new File(dir, fileName);
        try{
            file.createNewFile();
        }catch(IOException e) {e.printStackTrace(); }

        return file;
    }
}