package com.variable.applications.clima.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.variable.CollectionUtility;
import com.variable.applications.clima.R;
import com.variable.framework.node.NodeDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    @Override

    public boolean areAllItemsEnabled() {   return true;    }

    private Context context;
    private ArrayList<Object> groups;
    private List<CopyOnWriteArraySet<ChildItem>> children;
    private final Handler mHandler;
    public ExpandableListAdapter(Context context, ArrayList<Object> groups,
                                                  ArrayList<CopyOnWriteArraySet<ChildItem>> children) {
        this.context = context;
        this.groups = groups;
        this.children = Collections.synchronizedList(children);
        this.mHandler = new Handler();
    }

    public void postNotifyOnDataSetChanged(){
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();;
            }
        });
    }


    /**
     * A general add method, that allows you to add a Child Items to end of the list of children.
     *
     * Depending on if the category is present or not,
     * the corresponding item will either be added to an existing group if it
     * exists, else the group will be created and then the item will be added
     *
     * The dataset notification will be done on the UI thread will the actual operation occurs on the caller's thread.
     * @param item
     * */
    public void addItem(final ChildItem item) {
        int index = groups.indexOf(item.getGroup());
        addGroupItem(index, item.getGroup());

        Set<ChildItem> childs = children.get(index);
        if(!childs.contains(item))
        {
            children.get(index).add(item);
        }

        postNotifyOnDataSetChanged();

    }

    /**
     * General Method to allow for removing a child item, if present.
     * @param item - the child to remove.
     */
    public void removeItem(final ChildItem item){
        int index = groups.indexOf(item.getGroup());
        if (children.size() < index + 1) {  return; }

        //Remove the item from the list of children.
        children.get(index).remove(item);

        postNotifyOnDataSetChanged();
    }


    public void addGroupItem(final int index, final Object group){
             if (!groups.contains(group)) {

                    //Add the group to the list of groups
                    groups.add(index, group);

                    //Add a set for this  group in the children list.
                    children.add(index, new CopyOnWriteArraySet<ChildItem>());
                }
        //Update on UI Thread.
        postNotifyOnDataSetChanged();
    }

    public void addGroupItem(final Object group){
        addGroupItem(groups.size(), group);
    }

    /**
     * Removes a group from the list of groups.
     * Removes the corresponding child list for the group.
     * @param group
     */
    public void removeGroupItem(final Object group) {

        //Gets the Index of the group to be removed.
        int index = 0;
        if (groups.get(index).equals(group)) {
            //Handles the first case so that the loop can pre increment.

        } else {
            while (++index < groups.size() && groups.get(index).equals(group)) ;
        }

        //Remove the group
        if(groups.remove(group)){

            //Remove all belonging children to the group being removed.
            children.remove(index);
        }

        postNotifyOnDataSetChanged();


    }
    public void removeGroupAt(int index){

    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return CollectionUtility.ElementAt(children.get(groupPosition), childPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    // Return a child view. You can load your custom layout here.
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        ChildItem child = (ChildItem) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_row, null);

        }
        TextView tv = (TextView) convertView.findViewById(R.id.childText);
        tv.setText("   " + child.getText());

        if(child.getImageId() != null){
            ImageView view = (ImageView) convertView.findViewById(R.id.childImage);
            //Gets the drawable for the view from the Child Item.
            Drawable image = context.getResources().getDrawable(child.getImageId());

            //After API Level 16 the setBackgroundDrawable was deprecated.
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1){
                view.setBackgroundDrawable(image);
            }else{
                view.setBackground(image);
            }

            view.setVisibility(View.GONE);
        }

        //TODO:  Depending upon the child type, set the imageTextView01
        if(child.getSource() instanceof NodeDevice){
            //Change Background if Connected.
            NodeDevice node = (NodeDevice) child.getSource();
            if(node.isConnected()){ convertView.findViewById(R.id.imgExtra).setVisibility(View.VISIBLE);   }
            else                  { convertView.findViewById(R.id.imgExtra).setVisibility(View.GONE); }
        }
        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return children.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) { return groups.get(groupPosition);   }

    @Override
    public int getGroupCount() {    return groups.size();   }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    // Return a group view. You can load your custom layout here.
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        String group = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_row, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.group_row_text);
        tv.setText(group);
        return convertView;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }


    public static class ChildItem implements Comparable{
        private final String text;
        private final Integer imageId;
        private final Object source;
        private final String group;

        public ChildItem(String group, String text, Integer imageId, Object source) {
            this.text = text;
            this.imageId = imageId;
            this.source = source;
            this.group  = group;
        }


        public String   getGroup()   {   return group;   }
        public String    getText()    {   return text;    }
        public Integer   getImageId() {   return imageId; }
        public Object   getSource()  {   return source;  }

        @Override
        public boolean equals(Object o) {
            if(o == this){ return true; }
            else if(o instanceof  ChildItem){
                ChildItem item = (ChildItem) o;

                boolean results = item.getGroup().equals(group);
                results &= item.getSource() == source;
                return results;
            }

            return false;
        }

        @Override
        public int compareTo(Object o) {
           if(equals(o)){   return 0; }
           else if(o instanceof ChildItem){
               return ((ChildItem) o).getText().compareTo(text);
           }else{
               return -1;
           }
        }
    }
}
