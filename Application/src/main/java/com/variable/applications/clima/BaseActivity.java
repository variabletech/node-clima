package com.variable.applications.clima;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.variable.applications.clima.adapters.CustomListViewAdapter;
import com.variable.applications.clima.adapters.ExpandableListAdapter;
import com.variable.applications.clima.bluetooth.BaseBluetoothActivity;
import com.variable.applications.clima.bluetooth.DeviceFilter;
import com.variable.applications.clima.fragment.RecordingViewerFragment;
import com.variable.framework.node.NodeDevice;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArraySet;


/**
 *
 * BaseActivity handles bluetooth connection and disconnect. Additionally, handles navigation drawers for the left and right side.
 *
 * The navigation drawer for the right side provides the feature of navigating to any NODE demo applications. However, if the device running this activity
 * does not have the application installed on device then the activity will launch an intent for the app store to download the selected application.
 * When the an item is selected from this drawer the onAppSelected(String appName) will be invoked.
 *
 * The navigation drawer for the left side is to provide a list of all available NODE devices. The viewDeviceDrawer will allow control of opening and closing this drawer.
 * Additionally, when an item is selected from this drawer the onNodeSelected(node) method will be invoked.
 *
 *
 *
 *
 */
public abstract class BaseActivity extends BaseBluetoothActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    /**
     * This key is responsible for holding the location in the navigation drawer. Furthermore, the children are all the currently scanned NODE devices.
     */
    public static final String NODE_DEVICES_KEY = " Paired Nodes";

    /**
     * This key is responsible for displaying the settings in the navigation drawer.
     * If the consumer does not need settings functionality, they can call removeGroupName(SETTING_GROUP_NAME_KEY), to remove it.
     *
     */
    public static final String SETTINGS_GROUP_NAME_KEY =  " Settings ";


    public static final String VIEW_RECORDING_GROUP_NAME = " View Recordings ";

    private DrawerLayout          mDrawerLayout;
    private ActionBarDrawerToggle mNodeDrawerToggle;
    private CharSequence          mTitle;

    //Action Drawer Members for the left drawer
    //private LinkedList<String> mNodeAddressTitles = new LinkedList<String>();
    private ExpandableListView mNavDrawerExpandableListView;
    protected ExpandableListAdapter mNavDrawerExpandableListAdapter;
    private CharSequence mNodeDrawerTitle = "Nodes";

    //Action Drawer Members for the left drawer
    private ListView mNodeAppDrawerList;


    private CharSequence mNodeAppDrawerTitle = "Marketplace";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        //Request for the use of the action bar.
        requestWindowFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.base_activity);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        if(getActionBar() != null){
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mNodeDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                //mNodeDrawerLayout,         /* DrawerLayout object */
                mDrawerLayout,
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            public void onDrawerOpened(View drawerView) {
                CharSequence title = (drawerView.getId() == R.id.left_drawer) ? mNodeDrawerTitle : mNodeAppDrawerTitle;
                getActionBar().setTitle(title);

                if(drawerView.getId() == R.id.left_drawer){
                    if(mDrawerLayout.isDrawerOpen(mNodeAppDrawerList)){
                        mDrawerLayout.closeDrawer(mNodeAppDrawerList);
                    }

                    //ExpandableListAdapter adapter = (ExpandableListAdapter) mNavDrawerExpandableListView.getExpandableListAdapter();
                    //if(adapter.getChildrenCount(0) == 0){
                    //    BluetoothAdapter.getDefaultAdapter().startDiscovery();
                    //}

                }else if(drawerView.getId() == R.id.right_drawer){
                        viewDeviceDrawer(false);
                }

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }
        };
        mDrawerLayout.setDrawerListener(mNodeDrawerToggle);


        mNodeAppDrawerList = (ListView) findViewById(R.id.right_drawer);
        mNodeAppDrawerList.setAdapter(getMarketPlaceAdapter());
        // Set the list's click listener
        mNodeAppDrawerList.setOnItemClickListener(new DrawerItemClickListener());



        mTitle =  getTitle();
        mNavDrawerExpandableListView = (ExpandableListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
       mNavDrawerExpandableListAdapter  = new ExpandableListAdapter(
                this,
                new ArrayList<Object>(),
                new ArrayList<CopyOnWriteArraySet<ExpandableListAdapter.ChildItem>>()
        );

        mNavDrawerExpandableListView.setAdapter(mNavDrawerExpandableListAdapter);
        mNavDrawerExpandableListView.setGroupIndicator(null);

        //Add the NODE_DEVICE to Group Item.
        mNavDrawerExpandableListAdapter.addGroupItem(NODE_DEVICES_KEY);
        if(((BaseApplication) getApplication()).isRecordingAllowed()){
            mNavDrawerExpandableListAdapter.addGroupItem(VIEW_RECORDING_GROUP_NAME);
        }
        mNavDrawerExpandableListAdapter.addGroupItem(SETTINGS_GROUP_NAME_KEY);

        // Set the list's click listener
        mNavDrawerExpandableListView.setOnChildClickListener(new DrawerItemClickListener());
        mNavDrawerExpandableListView.setOnGroupClickListener(new DrawerItemClickListener());

        setDeviceFilter(new DeviceFilter() {
            @Override
            public boolean accept(BluetoothDevice device) {
                boolean result = device.getBluetoothClass() != null && device.getBluetoothClass().getDeviceClass() == 144;
                return result || (device.getBluetoothClass() != null && device.getBluetoothClass().getDeviceClass() == 7936);

            }
        });

        super.onCreate(savedInstanceState);

    }


    private ArrayAdapter<CustomListViewAdapter.RowItem> getMarketPlaceAdapter(){
        CustomListViewAdapter adapter = new CustomListViewAdapter(this, R.layout.drawer_list_item);

        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_node_app, getString(R.string.node), "Manage Settings"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_motion, getString(R.string.motion), "Motion Sensing"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_clima, getString(R.string.clima),  "Ambient Sensing"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_chroma, getString(R.string.chroma), "Color Matching"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_therma, getString(R.string.therma), "Temperature Sensing"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_luma, getString(R.string.luma), "The Flashlight"));
        adapter.add(new CustomListViewAdapter.RowItem(R.drawable.ic_app_oxa, getString(R.string.oxa), "Gas Sensing"));
        return adapter;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        //Only Show the Recording Icon if the application allows recording.
        boolean isRecordingAllowed = ((BaseApplication) getApplication()).isRecordingAllowed();
        menu.findItem(R.id.action_recording).setVisible(isRecordingAllowed);

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(super.onPrepareOptionsMenu(menu)){
            return true;
        }else{
            // If the nav drawer is open, hide action items related to the content view
            boolean drawerOpen = mDrawerLayout.isDrawerOpen(mNavDrawerExpandableListView) | mDrawerLayout.isDrawerOpen(mNodeAppDrawerList);
            MenuItem action_apps_item = menu.findItem(R.id.action_apps);
            if(action_apps_item != null){
                action_apps_item.setVisible((mDrawerLayout.isDrawerOpen(mNodeAppDrawerList)) || !drawerOpen);
            }
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mNodeDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle action buttons
       else  if(R.id.action_apps == item.getItemId()) {
                if(mDrawerLayout.isDrawerOpen(mNodeAppDrawerList)){
                    mDrawerLayout.closeDrawer(mNodeAppDrawerList);
                }else{
                    mDrawerLayout.openDrawer(mNodeAppDrawerList);
                }
                return true;

        }else if(R.id.action_recording == item.getItemId()){
            //Alert the user of the change and updated the Icon.
            boolean startRecording = !((BaseApplication) getApplication()).isRecording();
            Drawable recordingDrawable;
            String toastMessage;
            if(startRecording){
                //Get the Drawable for when recording is active
                recordingDrawable = getResources().getDrawable(R.drawable.ic_recording_on);

                //Get the Message for starting recording.
                toastMessage = ((BaseApplication) getApplication()).isConnected() ? "Recording is started to group " + ((BaseApplication) getApplication()).getActiveRecording().getRecordingName() : " Recording will start upon connection";

                //Alerts the Application to begin recording.
                ((BaseApplication) getApplication()).onRecordingStart();
            }else{

                //Alerts the Application to stop recording.
                ((BaseApplication) getApplication()).onRecordingStop();

                recordingDrawable = getResources().getDrawable(R.drawable.ic_recording_off);
                toastMessage = " Recording has stopped";
            }

            Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
            item.setIcon(recordingDrawable);
            return true;

       }else{ return false; }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mNodeDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mNodeDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     *Allows for controlling the left drawer representing the node devices.
     */
    public void viewDeviceDrawer(boolean open){
        if(open){
            mDrawerLayout.openDrawer(mNavDrawerExpandableListView);
        }else{
            mDrawerLayout.closeDrawer(mNavDrawerExpandableListView);
        }
    }



    /**
     *
     * Selected Device handles when a device has been selected from the left navigation drawer.
     * It will initiate a new connection to the selected node.
     *
     *
     * @param node - the node that is selected.
     */
    public void onNodeSelected(NodeDevice node){

        ((BaseApplication) getApplication()).setActiveNode(node);



        //Initiate A Connection
        if(node.isConnected()){
            node.disconnect();
        }else{
            //Cancel Any Existing Connections.
            if(BaseApplication.getService().connectionCount() > 0)
                BaseApplication.getService().stop();

            node.connect();
        }


    }

    /**
     * Occurs when an application has been selected from the market place.
     * Furthermore, will launch the activity based on the app name.
     * @param appName
     */
    public void onSelectedApp(String appName){

      //Stop all connections after the intent is created, to get the connected device.
      BaseApplication.getService().stop();

      ActivityLaunchHelper.startActivityByAppName(this, appName);

    }


    /**
     * Sets the Title on the action bar.
     * @param title
     */
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        mTitle = title;
        if(getActionBar() != null){
            getActionBar().setTitle(mTitle);
        }
    }


    @Override
    public void onCommunicationInitCompleted(NodeDevice node) {
        super.onCommunicationInitCompleted(node);

       ((BaseApplication) getApplication()).setActiveNode(node);


       //close the drawer.
       viewDeviceDrawer(false);

       mNavDrawerExpandableListAdapter.postNotifyOnDataSetChanged();

    }

    public boolean onGroupItemSelected(String name){   return false;  }
    public void onItemSelected(ExpandableListAdapter.ChildItem item){   }


    @Override
    public void onNodeDiscovered(NodeDevice node) {
        super.onNodeDiscovered(node);

        ExpandableListAdapter.ChildItem item = new ExpandableListAdapter.ChildItem(NODE_DEVICES_KEY, node.getName(), R.drawable.ic_bluetooth, node);
        mNavDrawerExpandableListAdapter.addItem(item);
    }

    @Override
    public void onDiscoveryStarted(){
        super.onDiscoveryStarted();

        //Clear out the discovered nodes.
        mNavDrawerExpandableListAdapter.removeGroupItem(NODE_DEVICES_KEY);
        mNavDrawerExpandableListAdapter.addGroupItem(0, NODE_DEVICES_KEY);
    }

    @Override
    public void onDiscoveryCompleted() {
        super.onDiscoveryCompleted();

       viewDeviceDrawer(true);
    }


    /**
     * Notifies the left navigation drawer that the data set has changed.
     * @param node
     */
    @Override
    public void onDisconnect(NodeDevice node){
        super.onDisconnect(node);

        mNavDrawerExpandableListAdapter.postNotifyOnDataSetChanged();

        viewDeviceDrawer(true);
    }



    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener,ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // update selected item and title, then close the drawer
                mNodeAppDrawerList.setItemChecked(position, true);
                CustomListViewAdapter.RowItem item = ((CustomListViewAdapter) mNodeAppDrawerList.getAdapter()).getItem(position);
                mDrawerLayout.closeDrawer(mNodeAppDrawerList);
                onSelectedApp(item.getTitle());
        }

        @Override
        public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {

            ExpandableListAdapter.ChildItem item = (ExpandableListAdapter.ChildItem) (expandableListView.getExpandableListAdapter()).getChild(groupPosition, childPosition);
            if(item.getSource() instanceof NodeDevice){
                NodeDevice node = (NodeDevice) item.getSource();
                onNodeSelected(node);

                setTitle(node.getName());
                viewDeviceDrawer(false);
                return true;

            }else{
                onItemSelected(item);
                return true;
            }
        }


        @Override
        public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long id) {
            ExpandableListAdapter adpater = (ExpandableListAdapter)expandableListView.getExpandableListAdapter();
            if(adpater == null || adpater.getGroup(groupPosition) == null) { return false; }
            String name = adpater.getGroup((groupPosition)).toString();

            //Handles the case when "Paired Nodes" is selected and there are no paired NODES found.
            //If the user choose it will take them to the bluetooth settings screen.
            if(name.equals(NODE_DEVICES_KEY) && adpater.getChildrenCount(groupPosition) == 0){
               new  AlertDialog.Builder(BaseActivity.this)
                       .setTitle("No Paired NODEs")
                       .setMessage("There is no paried bluetooth devices that match NODE specifications \n\nGo and Pair with your NODE now?")
                       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {
                               Intent settingsIntent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                               startActivity(settingsIntent);
                           }
                       })
                       .setNegativeButton("No", null)
                       .create()
                       .show();
            }

            //Invoke the settings has been pressed.
            if(name.equals(SETTINGS_GROUP_NAME_KEY)){
                onSettingsMenuItemPressed();

                //Close Device Drawer
                viewDeviceDrawer(false);

                adpater.postNotifyOnDataSetChanged();
            }

            if(name.equals(VIEW_RECORDING_GROUP_NAME)){

                viewDeviceDrawer(false);

               return onRecordingsItemPressed();
            }
            //After User Click Close Device Driver, if handled.
            if(onGroupItemSelected(name)){

                viewDeviceDrawer(false);

                adpater.postNotifyOnDataSetChanged();

                return true;
            }else{
                return false;
            }


        }
    }


    @Override
    public void onBackPressed(){
        if(!getFragmentManager().popBackStackImmediate()){
            BaseApplication.getService().stop();

            finish();
        }
    }

    /**
     * Builds an intent to start the Recording Activity.
     */
    public  boolean onRecordingsItemPressed(){
        Intent intent = new Intent(this, RecordingViewerFragment.class);
        startActivity(intent);
        return true;
    }

    /**
     * Invoked when the settings menu is pressed.
     */
    public abstract void onSettingsMenuItemPressed();

    /**
     * Checks if a fragment with the specified tag exists already in the Fragment Manager. If present, then removes fragment.
     *
     * Animates out to the specified fragment.
     *
     *
     * @param frag
     * @param tag
     */
    public void animateToFragment(final Fragment frag, final String tag){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment existingFrag = getFragmentManager().findFragmentByTag(tag);
        if(existingFrag != null){
            getFragmentManager().beginTransaction().remove(existingFrag).commit();
        }

        ft.replace(R.id.center_fragment_container, frag, tag);
        ft.addToBackStack(null);
        ft.commit();
    }

}


