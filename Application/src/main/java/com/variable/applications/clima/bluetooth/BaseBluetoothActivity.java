package com.variable.applications.clima.bluetooth;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.variable.applications.clima.ClimaApplication;
import com.variable.framework.android.bluetooth.BluetoothService;
import com.variable.framework.android.bluetooth.DefaultBluetoothDevice;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.AndroidNodeDevice;
import com.variable.framework.node.NodeDevice;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * The BaseBluetoothActivity is a layoutless fragment activity. Furthermore, this provides the framework for all bluetooth related events.
 *
 * It is responsible for connecting, disconnect, listenning, and discovering devices.
 * Created by coreymann on 7/3/13.
 */
public class BaseBluetoothActivity extends Activity implements NodeDevice.ConnectionListener {
    public static final String TAG = BaseBluetoothActivity.class.getName();
    private static final int REQUEST_USER_BLUETOOTH_PAIRING = 2500;

    /**Preference key for detecting a previous connected device. **/
    public static final String PREVIOUS_CONNECTED_DEVICE_PREF = "com.variable.android.preference.PREVIOUS_CONNECTED_DEVICE";

    /**Allows for specifying a time out period for allowing a connection attempt**/
    public int mConnectionTimeout = 2500;
    private ProgressDialog mProgressDialog;
    private Dialog mExtraDialog;

    private DeviceFilter mDeviceFilter;


    private Timer bluetoothStreamFixTimer;
    /**
     * Initializes the BluetoothService, such that BaseApplication.getServiceAPI() != null.
     *
     * Performs a check on the calling intent for if the key BluetoothService.DISCOVERED_DEVICES_LIST is present.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setup Bluetooth Service
        BluetoothService service = new BluetoothService();
        ClimaApplication.setServiceAPI(service);
        mDefaultDevice = new DefaultBluetoothDevice(service);
        Intent i = getIntent();
        BluetoothDevice prevConnectedDevice= i.getParcelableExtra(BluetoothService.EXTRA_DEVICE);
        if(prevConnectedDevice  != null){
            Log.d(TAG, "onCreate() ------ Saving Previously Connected Device");

            //Store in preferences
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString(PREVIOUS_CONNECTED_DEVICE_PREF, prevConnectedDevice.getAddress()).commit();
        }

        DefaultNotifier.instance().addConnectionListener(this);
    }


    @Override
    protected  void onDestroy(){
        super.onDestroy();
        DefaultNotifier.instance().removeConnectionListener(this);
    }

    @Override
    protected void onResume(){
        super.onResume();

        addPairedNodes();

        //Always ensure Bluetooth is on before processing.
        ensureBluetoothIsOn();



        String previousAddress = PreferenceManager.getDefaultSharedPreferences(this).getString(PREVIOUS_CONNECTED_DEVICE_PREF, "NO NODE");
        if(!previousAddress.equals("NO NODE") && isAutoConnect()){
            BluetoothDevice prevConnectedDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(previousAddress);

            //Add to the NodeDevice manager.
            NodeDevice node =  AndroidNodeDevice.getOrCreateNodeFromBluetoothDevice(prevConnectedDevice, mDefaultDevice);

            ((ClimaApplication) getApplication()).setActiveNode(node);

            onNodeDiscovered(node);

            if(node.isConnected())
            {
                node.disconnect();
            }
            else
            {
                //Restart Listening Server......
                ClimaApplication.getService().start();
                node.connect();
            }
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(TAG, "========onPause=======");

        NodeDevice node = ((ClimaApplication) getApplication()).getActiveNode();
        if((node != null && node.isConnected())){
            node.disconnect();
        }
    }

    private void addPairedNodes(){

        //Add the Bonded/Paired devices.
        onDiscoveryStarted();
        Set<BluetoothDevice> previousDiscoveredDev = BluetoothAdapter.getDefaultAdapter().getBondedDevices();

        for(BluetoothDevice discoveredDevice : previousDiscoveredDev){

            if(mDeviceFilter == null) {
                NodeDevice node = AndroidNodeDevice.getOrCreateNodeFromBluetoothDevice(discoveredDevice, mDefaultDevice);
                ((ClimaApplication) getApplication()).mDiscoveredDevices.add(discoveredDevice);
                onNodeDiscovered(node);

            }else if(mDeviceFilter.accept(discoveredDevice)){
                NodeDevice node = AndroidNodeDevice.getOrCreateNodeFromBluetoothDevice(discoveredDevice, mDefaultDevice);
                //Discovered new NODE.
                ((ClimaApplication) getApplication()).mDiscoveredDevices.add(discoveredDevice);
                onNodeDiscovered(node);
            }
        }

        onDiscoveryCompleted();
    }

    /**
     * Sets the timeout period from when the connection begins to communication init completed.
     * @param connectionTimeout
     */
    public void setConnectionTimeout(int connectionTimeout){ mConnectionTimeout = connectionTimeout; }

    @Override
    public void onCommunicationInitCompleted(final NodeDevice node) {
        if(mProgressDialog != null){
            mProgressDialog.cancel();
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }

        //Fix for NON Samsung Devices.
        //Without it the BluetoothInputStream lags after 3 - 5 seconds of streaming.
        bluetoothStreamFixTimer = new Timer("Bluetooth Fix");
        bluetoothStreamFixTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                node.sendString("$");
            }
        }, 0, 2500);
    }

    @Override
    public void onConnected(final NodeDevice node) {
        Log.d(TAG, "onConnected(node) " + node.getName());

        buildDialog(node, "Initializing...");

        //Save Device as Last Connected.
        if(isAutoConnect()){
            PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putString(PREVIOUS_CONNECTED_DEVICE_PREF, node.getAddress())
                    .commit();
        }
    }

    protected void dismissDialog() {
        if(mExtraDialog != null && mExtraDialog.isShowing()){
            try{ mExtraDialog.dismiss(); } catch (Exception e) {e.printStackTrace(); }
        }

        if(mProgressDialog != null && mProgressDialog.isShowing()){
            try{mProgressDialog.dismiss(); } catch(Exception e) {e.printStackTrace(); }
        }
    }

    @Override
    public void onConnectionFailed(final NodeDevice node, Exception ex){
        Log.d(TAG, "onConnectionFailed");

        dismissDialog();
        mExtraDialog = new AlertDialog.Builder(this)
                .setTitle("Connection Failed")
                .setMessage("If connection problems persist, try the following:\n\n" +
                        "- Ensure that your NODE is fully charged\n\n" +
                        "- Turn on or restart your NODE\n\n" +
                        "- Restart Bluetooth on your Android Device\n\n" +
                        "- Unpair and then re-Pair with your NODE in Android's Bluetooth Settings\n\n" +
                        "- Restart both your Android Device and your NODE\n\n")
                .setPositiveButton("Close", null)
                .setNeutralButton("Retry Connection", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        node.connect();
                    }
                })
                .setNegativeButton("Go to Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                        startActivityForResult(intent, REQUEST_USER_BLUETOOTH_PAIRING);
                    }
                })
                .create();
        try{ mExtraDialog.show(); } catch(Exception e) {e.printStackTrace(); }
    }


    @Override
    public void onDisconnect(NodeDevice node) {
        dismissDialog();

        Toast.makeText(this, node.getName() + " disconnected", Toast.LENGTH_SHORT).show();
        ((ClimaApplication)getApplication()).setActiveNode(node);

        if(bluetoothStreamFixTimer != null){
            bluetoothStreamFixTimer.purge();
            bluetoothStreamFixTimer = null;
        }
    }

    @Override
    public void onNodeDiscovered(NodeDevice node) { }

    /**
     * @deprecated
     */
    public void onDiscoveryStarted(){    }

    /**
     * @deprecated
     */
    public void onDiscoveryCompleted() {    }

    @Override
    public void nodeDeviceFailedToInit(NodeDevice node) {
        Toast.makeText(this, node.getName() + " failed to init.", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Node Failed to Init Communication");

        dismissDialog();
        node.disconnect();
    }

    @Override
    public void onInitializationUpdate(String s) {
        if (mProgressDialog != null) {
            try {
                mProgressDialog.setMessage(s);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onConnecting(NodeDevice nodeDevice) {
        buildDialog(nodeDevice, "Connecting...");
    }


    private void ensureBluetoothIsOn(){
        if(!BluetoothAdapter.getDefaultAdapter().isEnabled()){
            Intent btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            btIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(btIntent);
        }
    }

    /**
     * Overriding allows control of auto-connecting to the address stored in the preference key PREVIOUS_CONNECTED_DEVICE_PREF
     * @return
     */
    public boolean isAutoConnect() { return false; }


    public void setDeviceFilter(DeviceFilter filter){   mDeviceFilter = filter; }

    private final void buildDialog(final NodeDevice node,  String message){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(BaseBluetoothActivity.this);
        }

        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                node.disconnect();
            }
        });

        mProgressDialog.setTitle("Bluetooth Connection");
        mProgressDialog.setMessage(message);

        if(!mProgressDialog.isShowing()) { mProgressDialog.show(); }
    }

    private DefaultBluetoothDevice mDefaultDevice;
    private NodeDevice mActiveNode;
}