package com.variable.applications.clima.database;

import java.util.Date;

/**
 *
 * RecordingEntity is an entity that represents the data in conjunction with a single record from Recordings table.
 *
 */
public class RecordingEntity {
    private final int id;
    private final String recordingName;
    private final Date creation;

    public RecordingEntity(int id, String recordingName, Date creation) {
        this.id = id;
        this.recordingName = recordingName;
        this.creation = creation;
    }

    public Date getCreationDate(){
        return this.creation;
    }
    public int getId() {
        return id;
    }

    public String getRecordingName() {
        return recordingName;
    }

    @Override
    public String toString(){
        return recordingName;
    }
}
