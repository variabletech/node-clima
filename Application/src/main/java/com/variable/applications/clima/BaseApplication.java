package com.variable.applications.clima;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;

import com.variable.applications.clima.database.RecordingEntity;
import com.variable.framework.android.bluetooth.BluetoothService;
import com.variable.framework.node.NodeDevice;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by coreymann on 6/10/13.
 */
public abstract class BaseApplication extends Application {

    private static BluetoothService mBluetoothService;
    public  final ArrayList<BluetoothDevice> mDiscoveredDevices = new ArrayList<BluetoothDevice>();
    public         NodeDevice mActiveNode;
    private boolean isRecording;
    protected boolean isRecordingAllowed = true;


    public static final BluetoothService getService(){
        return mBluetoothService;
    }

    public static final BluetoothService setServiceAPI(BluetoothService api){
        mBluetoothService = api;
        return mBluetoothService;
    }

    public void setActiveNode(NodeDevice node){ mActiveNode = node; }

    public NodeDevice getActiveNode(){  return mActiveNode; }

    public boolean isConnected() { return mActiveNode != null && mActiveNode.isConnected(); }

    public boolean isRecordingAllowed() { return isRecordingAllowed; }

    /**
     *
     * @return true, if the application is currently recoding.
     */
    public  boolean isRecording(){  return isRecording; }


    public abstract RecordingEntity getActiveRecording();

    /**
     * Implementation is responsible for removing all readings that belong
     * to any of the recording groups from the list specified.
     *
     * @param recording the recordings that specify which readings to delete.
     * @return
     */
    public abstract int removeReadings(List<RecordingEntity> recording);

    /**
     * Implementation is responsible for creating an async task that will
     * retrieve all readings from all of the recording groups specified by the list in the execution parameter.
     * In addition to, dumping the contents of the data retrieval to the specified file.
     *
     * The async task should return true if all of the data contents were completely written to the file.
     *
     * @param f - the file that data will be written to.
     * @return
     */
    public abstract AsyncTask<List<RecordingEntity>, Boolean, Boolean> beginAsyncReadingRetrieving(File f);

    /**
     * Builds an AsyncTask responsible for retrieving all recordings currently available.
     * It will accept a list that then the async task will use to add each recording entity to.
     *
     * @return
     */
    public abstract AsyncTask<LinkedList<RecordingEntity>, Void, Void> beginAsyncRecordingRetrieving();
    /**
     * Invoked when recording is requested to start.
     * Updates recording status to true.
     */
    public void onRecordingStart(){
        this.isRecording = true;
    }

    /**
     * Invoked when recording is requested to stop.
     * Updates recording to not recording.
     */
    public  void onRecordingStop(){
        this.isRecording = false;
    }
}
