package com.variable.applications.clima;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import com.variable.applications.clima.database.ClimaContract;
import com.variable.applications.clima.database.ClimaDBHelper;
import com.variable.applications.clima.database.ClimaDeviceEntity;
import com.variable.applications.clima.database.RecordingEntity;
import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.DataLogSetting;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.adapter.DataLoggingAdapter;
import com.variable.framework.node.reading.SensorReading;
import com.variable.framework.node.reading.TimeStampReading;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Corey_2 on 9/6/13.
 */
public class DatalogDownloader extends DataLoggingAdapter  implements ClimaSensor.ClimaHumidityListener, ClimaSensor.ClimaPressureListener, ClimaSensor.ClimaTemperatureListener, ClimaSensor.ClimaLightListener {

    /**The MESSAGE what property in a general datalog update**/
    public static final int MESSAGE_DATALOG_UPDATE = 1000;

    /**The MESSSAGE what property in a datalog status update**/
    public static final int MESSAGE_DATALOG_STATUS = 2000;

    public static final int ARGUMENT_FETCHED_RECORDS_UPDATE   = 10;
    public static final int START_DATALOGGING_FETCH_ARGUMENT  = 20;
    public static final int FETCH_COMPLETED = 30;
    public static final int ARGUMENT_SHARE_DATA = 40;
    public static final int ARGUMENT_NO_RECORDS_FETCHED = 50;

    /**
     * The MESSAGE arg1 property is set to this when data log is currently running.
     */
    public static final int ARGUMENT_DATALOG_STATUS_RUNNING = 1;

    /**
     * Is sent with the MESSAGE_DATALOG_STATUS and represents when datalogging has been turned off.
     */
    public static final int ARGUMENT_NO_DATALOGGING = 0;

    private Date beginDate;
    private Date endDate;

    private final DataLogSetting dlog;
    private final DataLogSetting.DataType loggingType;
    private Handler mHandler;
    private int currentFetchedRecords;

    private final ClimaDeviceEntity deviceEntity;
    private RecordingEntity recordingEntity;
    private final Context context;
    private File dataFile;

    public DatalogDownloader(DataLogSetting dlog, DataLogSetting.DataType type, ClimaDeviceEntity entity, Context c) {
        this.dlog = dlog;
        loggingType = type;
        deviceEntity = entity;
        context = c;
    }

    public void setHandler(Handler h) { mHandler = h; }
    public DatalogDownloader setStartDate(Date date){
        beginDate = date;
        return this;
    }

    public DatalogDownloader setEndDate(Date date){
        endDate = date;
        return this;
    }

     /**
     * Begins a Fetch from the connected NODE based on the DataType associated with this instance.
     * If no date range is specified then all readings will be fetched for the given data type.
     * If no recording name has been specified then the default is used. "Dlog Export [Day, Month date Year hh:mm]"
     * @throws java.lang.IllegalArgumentException under two conditions
     *          1. Begin Date is after the End Date.
     *          2. The End Date is before the Begin Date.
      *         3. no recording entity has been set.
     */
    public  void beginExportData(){
        if(recordingEntity == null){
            throw new IllegalArgumentException("Their must be a recording entity set.");
        }

        if(endDate != null && beginDate != null){
            if(beginDate.after(endDate)){
                throw new IllegalArgumentException("The start date cannot be after the ending date.");
            }

            if(endDate.before(beginDate)){
                throw new IllegalArgumentException("The ending date cannot be before the start date");
            }

            mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, START_DATALOGGING_FETCH_ARGUMENT, -1).sendToTarget();
            dataFile = createDataFile();
            dlog.requestReadingsbyType(loggingType, beginDate, endDate);

        }else{
            //We must send the message before we start due to timing concerns.
            mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, START_DATALOGGING_FETCH_ARGUMENT,-1).sendToTarget();
            dataFile = createDataFile();
            dlog.requestReadingsbyType(loggingType, Short.MAX_VALUE);
        }
    }

    public void flipDatalogEnableFlag()
    {
        if(dlog.isAllowedState()){

            boolean newState = !dlog.isDataLoggingOn();
            if(newState){
                dlog.requestStart(new Date());
            }else{
                dlog.requestStop();
            }

        }else{
            throw new IllegalStateException("Unable to control data logging because it is not in an AllowedState.");
        }
    }

    /**
     * Sets the recording record for this datalog fetch.
     * @param entity
     */
    public void setRecordingEntity(RecordingEntity entity){
       recordingEntity =  entity;
    }

    public void cancelExport(){
        dlog.cancelFetchOperation();
    }

    public static final void showDatalogNotAllowedDialog(final Context c){
        new AlertDialog.Builder(c)
                .setTitle("Unable to Datalog")
                .setMessage("We are unable to datalog until datalogging is allowed. Please verify that datalogging is allowed in the myNODE app. Would you like to go there now?" )
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityLaunchHelper.startActivityByAppName(c, c.getString(R.string.node));
                    }
                })
                .setNegativeButton("Cancel", null)
                .create().show();
    }


    /**
     * Sends the MESSAGE_DATALOG_UPDATE with the arg1 set to ARGUMENT_DATALOG_STATUS. The actual status is set as the messages object.
     * @param device
     */
    @Override
    public void onStatusUpdated(NodeDevice device) {
        super.onStatusUpdated(device);

        boolean status = device.getDatalogSettings().isDataLoggingOn();
        int arg = ARGUMENT_NO_DATALOGGING;
        if(status){
            arg = ARGUMENT_DATALOG_STATUS_RUNNING;
        }

        mHandler.obtainMessage(MESSAGE_DATALOG_STATUS, arg, -1).sendToTarget();
    }

    /**
     * Sends the SHARE_DATA
     * @param device
     * @param op
     */
    @Override
    public void onOperationCompleted(NodeDevice device, DataLogSetting.Operation op) {
        super.onOperationCompleted(device, op);

        if(op == DataLogSetting.Operation.DataFetchComplete){
            if(dataFile != null){
                if(currentFetchedRecords > 0){
                    mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, ARGUMENT_SHARE_DATA,-1, dataFile.getAbsolutePath()).sendToTarget();
                }else if(currentFetchedRecords == 0){
                    mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, ARGUMENT_NO_RECORDS_FETCHED, -1).sendToTarget();
                }
            }
            currentFetchedRecords = 0;
            dataFile = null;
            mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, FETCH_COMPLETED, -1).sendToTarget();
        }
    }

    @Override
    public void onClimaHumidityUpdate(ClimaSensor sensor, SensorReading<Float> humidity) {
        if(humidity.getTimeStampSource() == TimeStampReading.TIME_STAMP_SOURCE_DATALOGGED)
            onRecievedReading(ClimaContract.ClimaDataTypes.HUMIDITY_DATA_TYPE_ID, humidity.getValue(), humidity.getTimeStamp());
    }

    @Override
    public void onClimaLightUpdate(ClimaSensor sensor, SensorReading<Float> light) {
        if(light.getTimeStampSource() == TimeStampReading.TIME_STAMP_SOURCE_DATALOGGED)
            onRecievedReading(ClimaContract.ClimaDataTypes.LIGHT_DATA_TYPE_ID, light.getValue(), light.getTimeStamp());
    }

    @Override
    public void onClimaPressureUpdate(ClimaSensor sensor, SensorReading<Integer> pressure) {
        if(pressure.getTimeStampSource() == TimeStampReading.TIME_STAMP_SOURCE_DATALOGGED)
            onRecievedReading(ClimaContract.ClimaDataTypes.PRESSURE_DATA_TYPE_ID, pressure.getValue(), pressure.getTimeStamp());
    }

    @Override
    public void onClimaTemperatureUpdate(ClimaSensor sensor, SensorReading<Float> temperature) {
        if(temperature.getTimeStampSource() == TimeStampReading.TIME_STAMP_SOURCE_DATALOGGED)
            onRecievedReading(ClimaContract.ClimaDataTypes.TEMPERATURE_DATA_TYPE_ID, temperature.getValue(), temperature.getTimeStamp());
    }



    private void onRecievedReading(int type, float reading, Date date){
        currentFetchedRecords++;
        mHandler.obtainMessage(MESSAGE_DATALOG_UPDATE, ARGUMENT_FETCHED_RECORDS_UPDATE, currentFetchedRecords).sendToTarget();
        ClimaDBHelper.instance(context).saveReading(deviceEntity.getId(), type, reading, date, recordingEntity.getId());

        //Ensure DataFile is available.
        if(dataFile == null){
            Log.d("", "Data File uninitialized not storing data. Type=" + type);  return; }


        final DateFormat formatter  = new SimpleDateFormat("MM-d-yyyy");
        final DateFormat timeFormatter = new SimpleDateFormat("hh:mm.ss");
        String newLine = formatter.format(date.getTime()) + ", " + timeFormatter.format(date.getTime())  + " , " + reading +  " , " + ClimaContract.getUnitSymbol(type) + "\r\n";
        writeReadingToFile(dataFile, newLine);
    }

    /**
     * Writes the reading to file.
     * @param file
     * @param value
     */
    private void writeReadingToFile(File file, String value){
        FileOutputStream f = null;
        try {

            f = new FileOutputStream(file, true);
            f.write(value.getBytes());

        } catch (IOException e) {   e.printStackTrace();    }
        finally {
               if(f != null){
                    try{      f.close();     } catch(IOException e) { e.printStackTrace(); }
                }
        }
    }

    private File createDataFile(){
        //Create the FilePath.
        SimpleDateFormat fileNameFormatter = new SimpleDateFormat("hh.mm.ss");
        String fileName =  "exportedData-" + fileNameFormatter.format(new Date().getTime()) + ".csv";
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + "/Variable/Clima/");
        dir.mkdirs();

        File file =  new File(dir, fileName);
        try{
               file.createNewFile();
        }catch(IOException e) {e.printStackTrace(); }

        return file;
    }
    /**
     * Request the current status of datalogging.
     * This will invoke the handler with MESSAGE_DATALOG_STATUS as its what in the message.
     */
    public void requestCurrentStatus() {
        dlog.requestState();
    }
}
