package com.variable.applications.clima.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.variable.applications.clima.LineGraph;
import com.variable.applications.clima.R;
import com.variable.applications.clima.BaseApplication;
import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;
import com.variable.framework.node.reading.SensorReading;

import org.achartengine.GraphicalView;

/**
 * Created by coreymann on 6/12/13.
 */
public class ClimaGraphFragment extends ClimaFragment {
    public static final String TAG = ClimaGraphFragment.class.getSimpleName();


    private LineGraph          mHumidityGraph;
    private GraphicalView      mHumidityGraphView;

    private LineGraph          mLightGraph;
    private GraphicalView      mLightGraphView;

    private LineGraph          mPressureGraph;
    private GraphicalView      mPressureGraphView;

    private LineGraph          mTempGraph;
    private GraphicalView      mTempGraphView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.clima, null, false);

        mHumidityGraph = new LineGraph("Humidity", "% RH",  "X Axis Title", "Y Axis Title", .10f);
        mPressureGraph = new LineGraph("Pressure", "kPA", "X Axis Title", "Y Axis Title", .005f);
        mLightGraph    = new LineGraph("Light", "lux" , "X Axis Title", "Y Axis Title", .10f);
        mTempGraph     = new LineGraph("Temperature", " C " , "X Axis Title", "Y Axis Title", .10f);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        mHumidityGraphView = mHumidityGraph.getView(getActivity());
        ((LinearLayout) getView().findViewById(R.id.humidityChart)).addView(mHumidityGraphView);

        mPressureGraphView = mPressureGraph.getView(getActivity());
        ((LinearLayout) getView().findViewById(R.id.pressureChart)).addView(mPressureGraphView);

        mTempGraphView     = mTempGraph.getView(getActivity());
        ((LinearLayout) getView().findViewById(R.id.temperatureChart)).addView(mTempGraphView);

        mLightGraphView    = mLightGraph.getView(getActivity());
        ((LinearLayout) getView().findViewById(R.id.lightChart)).addView(mLightGraphView);


        NodeDevice node = ((BaseApplication) getActivity().getApplication()).getActiveNode();
        if(node != null){
            ((ClimaSensor) node.findSensor(NodeEnums.ModuleType.CLIMA)).setStreamMode(true, true, true);
        }
    }


    @Override
    public void onPause(){
        super.onPause();

        NodeDevice node = ((BaseApplication) getActivity().getApplication()).getActiveNode();
        if(node != null){
            ((ClimaSensor) node.findSensor(NodeEnums.ModuleType.CLIMA)).setStreamMode(false, false, false);
        }

        ((LinearLayout) getView().findViewById(R.id.humidityChart)).removeView(mHumidityGraphView);
        ((LinearLayout) getView().findViewById(R.id.pressureChart)).removeView(mPressureGraphView);
        ((LinearLayout) getView().findViewById(R.id.temperatureChart)).removeView(mTempGraphView);
        ((LinearLayout) getView().findViewById(R.id.lightChart)).removeView(mLightGraphView);


        mHumidityGraphView = null;
        mPressureGraphView = null;
        mTempGraphView     = null;
        mLightGraphView    = null;

    }

    @Override
    public void onClimaHumidityUpdate(ClimaSensor sensor, SensorReading<Float> humidity) {
        mHumidityGraph.addPoint(humidity);
    }

    @Override
    public void onClimaLightUpdate(ClimaSensor sensor, SensorReading<Float> light) {
        mLightGraph.addPoint(light);
    }

    @Override
    public void onClimaPressureUpdate(ClimaSensor sensor, SensorReading<Integer> pressure) {
        SensorReading<Float> convertedReading = new SensorReading<Float>(pressure.getValue() * 1.0f/1000, pressure.getTimeStamp(), pressure.getTimeStampSource());
        mPressureGraph.addPoint(convertedReading);
    }

    @Override
    public void onClimaTemperatureUpdate(ClimaSensor sensor, SensorReading<Float> temperature) {
        mTempGraph.addPoint(temperature);
    }
}