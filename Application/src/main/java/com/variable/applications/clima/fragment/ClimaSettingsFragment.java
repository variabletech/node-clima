package com.variable.applications.clima.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.EditText;

import com.variable.applications.clima.ClimaApplication;
import com.variable.applications.clima.R;
import com.variable.applications.clima.database.ClimaDBHelper;
import com.variable.applications.clima.database.RecordingEntity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by coreymann on 6/6/13.
 */
public class ClimaSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {
     public static final String TAG = ClimaSettingsFragment.class.getName();

    private LinkedList<RecordingEntity> cachedRecordings;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onResume(){
        super.onResume();

        //Pull all the recordings from the DB.
        cachedRecordings = ClimaDBHelper.instance(getActivity()).getRecordings();
        cachedRecordings.addFirst(new RecordingEntity(cachedRecordings.size(),  getString(R.string.create_new_recording_group), new Date() ));
        String[] displayContents = new String[cachedRecordings.size()];
        for(int i = 0; i < cachedRecordings.size(); i++){
            displayContents[i] = cachedRecordings.get(i).toString();
        }

        //Setting the Entries and the EntryValues
        ListPreference chooseRecordingPref = (ListPreference) findPreference(getString(R.string.setting_recording_list));
        chooseRecordingPref.setEntries(displayContents);
        chooseRecordingPref.setEntryValues(displayContents);
        chooseRecordingPref.setOnPreferenceChangeListener(this);

        //Set the Current Recording as the ValueIndex.
        RecordingEntity current = ((ClimaApplication) getActivity().getApplication()).getActiveRecording();
        if(current != null){
            chooseRecordingPref.setValueIndex(chooseRecordingPref.findIndexOfValue(current.getRecordingName()));
        }

        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();

        PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if(preference.getKey() == getString(R.string.setting_recording_list)){

            //Allows User to Input a New Name to create the recording group with.
            if(newValue.toString().equals(getString(R.string.create_new_recording_group))){
                final EditText text = new EditText(getActivity());
                text.setHint("Enter a name to create a new recording group and set as the active recording group.");
                new AlertDialog.Builder(getActivity())
                        .setTitle("New Recording Group")
                        .setView(text)
                        .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(text.getText().length() > 0){
                                    //Get the Text of the recording group
                                    String newGroupName = text.getText().toString();

                                    //Write to the DB the new recording group
                                    RecordingEntity recordingEntity = ClimaDBHelper.instance(getActivity()).insertRecording(newGroupName);

                                    //Set the Application Current Recording Entity.
                                    ((ClimaApplication) getActivity().getApplication()).setRecordingEntity(recordingEntity);

                                    //Add to the Cache
                                    cachedRecordings.add(recordingEntity);
                                }
                            }
                        }).setNegativeButton("Cancel", null)
                        .create().show();
                return true;
            }else{
                //Search through the cached readings.
                for(RecordingEntity entity : cachedRecordings){
                    if(entity.getRecordingName().equals(newValue.toString())){
                        ((ClimaApplication) getActivity().getApplication()).setRecordingEntity(entity);
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
