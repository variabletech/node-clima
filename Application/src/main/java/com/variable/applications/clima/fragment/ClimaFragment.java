package com.variable.applications.clima.fragment;

import android.app.Fragment;

import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.reading.SensorReading;


/**
 * ClimaFragment is a layoutless fragment that provides listening for clima data.
 */
public class ClimaFragment extends Fragment implements ClimaSensor.ClimaHumidityListener,
                                                       ClimaSensor.ClimaTemperatureListener,
                                                       ClimaSensor.ClimaPressureListener,
                                                       ClimaSensor.ClimaLightListener {
    public static final String TAG = ClimaFragment.class.getSimpleName();

    @Override
    public void onResume(){
        super.onResume();

        DefaultNotifier.instance().addClimaTemperatureListener(this);
        DefaultNotifier.instance().addClimaPressureListener(this);
        DefaultNotifier.instance().addClimaHumidityListener(this);
        DefaultNotifier.instance().addClimaLightListener(this);

    }


    @Override
    public void onPause(){
        super.onPause();

        DefaultNotifier.instance().removeClimaHumidityListener(this);
        DefaultNotifier.instance().removeClimaPressureListener(this);
        DefaultNotifier.instance().removeClimaTemperatureListener(this);
        DefaultNotifier.instance().removeClimaLightListener(this);
    }


    @Override
    public void onClimaHumidityUpdate(ClimaSensor climaSensor, SensorReading<Float> floatSensorReading) {

    }

    @Override
    public void onClimaLightUpdate(ClimaSensor climaSensor, SensorReading<Float> floatSensorReading) {

    }

    @Override
    public void onClimaPressureUpdate(ClimaSensor climaSensor, SensorReading<Integer> integerSensorReading) {

    }

    @Override
    public void onClimaTemperatureUpdate(ClimaSensor climaSensor, SensorReading<Float> floatSensorReading) {

    }
}