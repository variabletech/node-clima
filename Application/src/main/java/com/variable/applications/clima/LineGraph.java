package com.variable.applications.clima;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;

import com.variable.framework.node.reading.SensorReading;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.NumberFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by coreymann on 6/13/13.
 */
public class LineGraph {
    private static final int MAX_POINTS = 80;
    public static final int X_INDEX = 0;


    private final TimeSeries              mXSeries        = new TimeSeries("");
    private final XYMultipleSeriesDataset mDataSet = new XYMultipleSeriesDataset();
    private XYMultipleSeriesRenderer      mRenderer;

    /** Graphical Content Members **/
    private GraphicalView       mChartView;

    /** This members control when graphical Components are updated  **/
    private final GraphUpdater  mUpdater;
    private       Timer         mTimer;

    private Float mScale = 0.0f;
    /**
     * Default Constructor
     */
    public LineGraph(){
        mDataSet.addSeries(X_INDEX, mXSeries);

        mRenderer = new XYMultipleSeriesRenderer();


        //User Input Control
        mRenderer.setClickEnabled(false);
        mRenderer.setPanEnabled(false);
        mRenderer.setZoomEnabled(false, false);

        //Colors And Padding
        mRenderer.setBarWidth(1.5f);
        mRenderer.setLabelsTextSize(25);

        //Y Label Formatting
        mRenderer.setYLabelsColor(0, Color.GRAY);

        mRenderer.setGridColor(Color.DKGRAY);
        mRenderer.setShowGridY(true);

        mRenderer.setXLabels(4);
        mRenderer.setYAxisAlign(Paint.Align.LEFT, 0);
        mRenderer.setXLabelsAlign(Paint.Align.LEFT);


        //Initialize Renders
        XYSeriesRenderer renderer = new XYSeriesRenderer();
            renderer.setChartValuesFormat(NumberFormat.getNumberInstance());
            renderer.setShowLegendItem(true);
            renderer.setLineWidth(4f);
            renderer.setColor(Color.WHITE);
            renderer.setFillBelowLine(true);
            renderer.setGradientEnabled(true);

            renderer.setGradientStart(0, Color.rgb(43,60,145));
            renderer.setGradientStop(MAX_POINTS, Color.rgb(36,148,207));
        mRenderer.addSeriesRenderer(X_INDEX, renderer);


        //Setup and Start the Timer for Collection.
        mUpdater = new GraphUpdater();

    }



    /**
     * Initializes a line graph with a chart title and a scale for the max and min Y values.
     * @param chartTitle
     * @param xAxisTitle
     * @param yAxisTitle
     * @param scale
     */
    public LineGraph(String chartTitle, String seriesName,  String xAxisTitle, String yAxisTitle, float scale){
        this();

        mXSeries.setTitle(seriesName);

        //Setting Chart Title and Size
        mRenderer.setChartTitle(chartTitle);
        mRenderer.setChartTitleTextSize(25f);
        mRenderer.setScale(scale);

        setPercentageScale(scale);

    }

    public XYMultipleSeriesDataset getDataSet() { return mDataSet; }

    public XYMultipleSeriesRenderer getRenderer() { return mRenderer; }


    /**
     * Returns a GraphicalView that represents the latest contents in the data set.
     * @return
     */
    public GraphicalView getView(Context context){
        if(mChartView == null){
            mChartView =  ChartFactory.getTimeChartView(context, mDataSet, mRenderer, "H:mm:ss");
        }
        return mChartView;
    }

    public void addPoint(SensorReading reading){
        if(mTimer == null){
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(mUpdater, 100,100);
        }
        mUpdater.addPoint(reading);


    }


    /**
     * The percentage scale controls the Y range. The scale is used to determine, MaxY * scale + MaxY and MinY * scale + MinY.
     * @param scale - percentage value from 0 - 1
     */
    public void setPercentageScale(float scale){
        //Use Synchronized due to timer threads are accessing.
        synchronized (mScale){
            mScale = scale;
        }
    }

    private class GraphUpdater extends TimerTask {
        private final ConcurrentLinkedQueue<SensorReading> mReadingsRecieved = new ConcurrentLinkedQueue<SensorReading>();
        private double minY = 0;
        private double maxY = 0;
        private final double[] mmRange = new double[]{ 0, MAX_POINTS, minY, maxY};

        @Override
        public void run() {
            while(!mReadingsRecieved.isEmpty()){

                if(mXSeries.getItemCount() >= MAX_POINTS){  mXSeries.remove(0); }

                //Get oldest reading in the queue.
                SensorReading  reading = mReadingsRecieved.poll();

                //Add Points to Series.
                mXSeries.add(reading.getTimeStamp(), Double.parseDouble(reading.getValue().toString()));

                scale();
            }

            mChartView.repaint();
        }


        public void addPoint(SensorReading reading){
            mReadingsRecieved.add(reading);
        }

        public void scale(){
            double maxY = mXSeries.getMaxY() + (mScale * mXSeries.getMaxY());
            double minY = mXSeries.getMinY() - (mScale * mXSeries.getMinY());

            mRenderer.setRange(new double[] { mXSeries.getMinX(), mXSeries.getMaxX(), minY,  maxY});

        }
    }
}
