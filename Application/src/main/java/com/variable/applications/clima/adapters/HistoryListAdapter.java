package com.variable.applications.clima.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.variable.applications.clima.R;
import com.variable.applications.clima.database.RecordingEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class HistoryListAdapter extends BaseAdapter implements Filterable {
    public static interface SelectedListener {
        public void firstSelectedItem();

        public void noSelectedItem();
    }
    private final Context ctx;
    private final LayoutInflater lInflater;
    private final List<RecordingEntity> arraylist;
    private static final SimpleDateFormat formatter = new SimpleDateFormat("MMM d yyyy hh:mm");
    private final Set<Integer> checkedPositions;
    private final List<RecordingEntity> allRecordings;
    private SelectedListener selectedListener;



    public HistoryListAdapter(Context context, List<RecordingEntity> recordings) {
        ctx = context;
        arraylist = recordings;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        checkedPositions = new HashSet<Integer>();
        allRecordings = new ArrayList<RecordingEntity>();
        allRecordings.addAll(recordings);
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setSelectedListener(SelectedListener listener) { selectedListener = listener; }
    private static final class ViewHolder{
        public TextView  txtCreationDate;
        public TextView  txtRecordingName;
        public CheckBox  checkBox;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        RecordingEntity entity  =  (RecordingEntity) getItem(position);

        if (view == null) {
            view = lInflater.inflate(R.layout.history_list_item, parent, false);
            holder = new ViewHolder();
            holder.txtCreationDate = (TextView) view.findViewById(R.id.secondLine);
            holder.txtRecordingName = (TextView) view.findViewById(R.id.firstLine);
            holder.checkBox         = (CheckBox) view.findViewById(R.id.list_item_check_box);
            holder.checkBox.setOnCheckedChangeListener(myCheckChangList);
            holder.checkBox.setTag(position);
        }else{
            holder = (ViewHolder) view.getTag();
        }


        holder.txtCreationDate.setText("Created On: " + formatter.format(entity.getCreationDate()));
        holder.txtRecordingName.setText(entity.getRecordingName());
        holder.checkBox.setChecked(checkedPositions.contains(position));
        view.setTag(holder);
        return view;
    }


    public ArrayList<RecordingEntity> getCheckedRecordings() {
        ArrayList<RecordingEntity> checkedRecordings = new ArrayList<RecordingEntity>();
        for(Integer checkedPosition : checkedPositions){
            checkedRecordings.add((RecordingEntity) getItem(checkedPosition));
        }

        return checkedRecordings;
    }

    @Override
    public Filter getFilter(){
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                // Now we have to inform the adapter about the new list filtered
                if (results == null || results.count == 0){
                    arraylist.clear();
                    notifyDataSetInvalidated();
                } else {

                    //Updated the Datasource
                    arraylist.clear();
                    arraylist.addAll((Collection<RecordingEntity>) results.values);
                    notifyDataSetChanged();
                }

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // We implement here the filter logic
                if (constraint == null || constraint.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = allRecordings;
                    results.count  = allRecordings.size();
                }

                else {
                    // We perform filtering operation
                    List<RecordingEntity> filterRecordings = new LinkedList<RecordingEntity>();

                    for(int i=0; i < getCount(); i++){
                        RecordingEntity recording = (RecordingEntity) getItem(i);
                        if(recording.getRecordingName().toUpperCase().contains(constraint.toString().toUpperCase())){
                            filterRecordings.add(recording);
                        }
                    }

                    results.values = filterRecordings;
                    results.count = filterRecordings.size();
                }

                return results;
            }
        };
    }

    private final CompoundButton.OnCheckedChangeListener myCheckChangList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            Integer position = (Integer) buttonView.getTag();
            if(checkedPositions.contains(position)){
                checkedPositions.remove(position);
            }else{
                checkedPositions.add(position);
            }

            if(checkedPositions.size() == 0){
                if(selectedListener != null)
                    selectedListener.noSelectedItem();
            }else if(checkedPositions.size() == 1){
                if(selectedListener != null)
                    selectedListener.firstSelectedItem();
            }
        }
    };
}