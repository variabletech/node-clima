package com.variable.applications.clima.bluetooth;

import android.bluetooth.BluetoothDevice;

/**
 * Created by coreymann on 7/3/13.
 */
public interface DeviceFilter {

    /**
     * Determines if the device should be accepted by the processor.
     * @param device
     * @return
     */
    public boolean accept(BluetoothDevice device);
}
