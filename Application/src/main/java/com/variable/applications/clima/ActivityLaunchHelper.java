package com.variable.applications.clima;

import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.variable.framework.android.bluetooth.BluetoothService;
import com.variable.framework.node.NodeDevice;

/**
 * Created by coreymann on 6/24/13.
 */
public class ActivityLaunchHelper {

    /**
     * Provides creating a new intent based on the appName.
     * @param context - the context
     * @param appName - the appname to create the intent for. Available names are located in this frameworks string resources.
     * @return
     */
    public static final void startActivityByAppName(Context context, String appName){

        Intent intent = new Intent();

        //Add the connected device to enable auto-start
        if(context.getApplicationContext() instanceof BaseApplication){

            NodeDevice node = ((BaseApplication) context.getApplicationContext()).getActiveNode();
            if(node != null && node.isConnected()){
                intent.putExtra(BluetoothService.EXTRA_DEVICE, BluetoothAdapter.getDefaultAdapter().getRemoteDevice(node.getAddress()));
            }
        }
        String action = null;
        String locationToMarket = "";
        if(appName.equals(context.getString(R.string.motion))){
            action = "com.variable.android.intent.action.STREAM_MOTION";
            locationToMarket ="market://details?id=com.variable.application.motion";
        }else if(appName.equals(context.getString(R.string.therma))){
            action = "com.variable.android.intent.action.STREAM_THERMA";
            locationToMarket = "market://details?id=com.variable.therma";
        }else if(appName.equals(context.getString(R.string.clima))){
            action = "com.variable.android.intent.action.STREAM_CLIMA";
            locationToMarket="market://details?id=com.variable.applications.clima";
        }else if(appName.equals(context.getString(R.string.chroma))){
            action = "com.variable.android.intent.action.SCAN_CHROMA";
            locationToMarket="market://details?id=com.variable.application.chroma";
        }else if(appName.equals(context.getString(R.string.node))){
            action="com.variable.android.intent.action.MANAGE_SETTINGS";
            locationToMarket="market://details?id=com.variable.application";
        }else if(appName.equals(context.getString(R.string.luma))){
            action=context.getString(R.string.luma_intent_action);
            locationToMarket="market://details?id=com.variable.application.luma";
        }else if(appName.equals(context.getString(R.string.oxa))){
            action=context.getString(R.string.oxa_intent_action);
            locationToMarket="market://details?id=com.variable.oxa.application";
        }else if(appName.equals(context.getString(R.string.thermocouple))){
            action = "com.variable.android.intent.action.STREAM_THERMOCOUPLE";
            locationToMarket="market://details?id=com.variable.thermocouple";
        }


        if(action == null) { return; }

        try{
            intent.setAction(action);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);

        }catch(ActivityNotFoundException e){
            e.printStackTrace();
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(locationToMarket));

            context.startActivity(intent);
        }
    }
}
