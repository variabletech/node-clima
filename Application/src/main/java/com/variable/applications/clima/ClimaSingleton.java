package com.variable.applications.clima;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.variable.applications.clima.database.ClimaDBHelper;
import com.variable.applications.clima.database.RecordingEntity;
import com.variable.framework.android.*;
import com.variable.framework.dispatcher.DefaultNotifier;
import com.variable.framework.node.BaseSensor;
import com.variable.framework.node.ClimaSensor;
import com.variable.framework.node.NodeDevice;
import com.variable.framework.node.enums.NodeEnums;
import com.variable.framework.node.reading.SensorReading;

/**
 * Created by Corey_2 on 9/3/13.
 */
public class ClimaSingleton implements ClimaSensor.ClimaHumidityListener, ClimaSensor.ClimaPressureListener, ClimaSensor.ClimaTemperatureListener, ClimaSensor.ClimaLightListener {
    private static final ClimaSingleton INSTANCE = new ClimaSingleton();
    public static ClimaSingleton instance() { return INSTANCE; }

    private ClimaDBHelper mDBHelper;
    private DatalogDownloader dlogDownloader;
    private RecordingEntity recordingEntity;
    private Context mContext;


    public static boolean containsClima(NodeDevice node)
    {
        return node.findSensor(NodeEnums.ModuleType.CLIMA) != null;
    }

    public static void showSensorNotFound(final BaseActivity c, final NodeDevice node){

        //Show Sensor Not Found Dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage("Attach Sensor and retry.")
                .setTitle("Sensor Not Found")
                .setIcon(R.drawable.ic_node_app)
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        node.disconnect();
                    }
                })
                .create()
                .show();
    }


    /**
     * Sets the recording name.
     * @param name if null, disables recording.
     */
    public void setRecordingName(String name){
        if(name == null) { recordingEntity = null; return; }

       recordingEntity = mDBHelper.insertRecording(name);
    }

    @Override
    public void onClimaHumidityUpdate(ClimaSensor sensor, SensorReading<Float> humidity) {

    }

    @Override
    public void onClimaLightUpdate(ClimaSensor sensor, SensorReading<Float> light) {

   }

    @Override
    public void onClimaPressureUpdate(ClimaSensor sensor, SensorReading<Integer> pressure) {

    }

    @Override
    public void onClimaTemperatureUpdate(ClimaSensor sensor, SensorReading<Float> humidity) {

    }
}
